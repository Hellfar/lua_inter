std_lua()
ProcessMod = true

local seen={}

function dump(t,i)
	if t==nil then
		print(type(t))
		return
	end
	seen[t]=true
	local s={}
	local n=0
	for k in pairs(t) do
		n=n+1 s[n]=k
	end
	-- table.sort(s)
	for k,v in ipairs(s) do
		print(i,v)
		v=t[v]
		if type(v)=="table" and not seen[v] then
			dump(v,i.."\t")
		else
			print(i, v)
		end
	end
end

function affDir(tab, t)
	local i = 1
	outflow("dirName = " .. tab['d_name'] .. "\t" .. #tab .. "\n")
	while i < (#tab + 1) do
		outflow(t .. i .. "\t")
		if type(tab[i]) == "string" then
			outflow(tab[i] .. "\n")
		elseif type(tab[i]) == "table" then
			affDir(tab[i], t .. "\t")
		end
		i = i + 1
	end
end

function main()
	allowConsole(true)
	outflow(__DIR__ .. "\n")
	test = listDir(".", true)
	affDir(test, "")
	pause("PRESS ENTER TO CONTINUE...")
	-- dump(test,"")
	-- os.execute("PAUSE")
	return (0)
end