std_lua()
ProcessMod = true

function main()
	local run = true
	while run do
		allowConsole(true)
		outflow(__DIR__ .. "\n")
		outflow("Welcome in game room !\nPlease type the directory game : ")
		dir = incflow()
		if dir == "q" or dir == "quit" or dir == "exit" then
			run = false
		else
			allowConsole(false)
			include(dir .. "/index.lua", false)
		end
	end
	-- os.execute("PAUSE")
	return (0)
end