function init_map()
	resource['head']['sizeTile'] = 16
	icone = IMG_Load(__DIR__ .. "/" .. game .. "/favico.png")
	SDL_SetColorKey(icone, SDL_SRCCOLORKEY, SDL_MapRGB(icone, 0x000000))
	SDL_WM_SetIcon(icone)
	SDL_WM_SetCaption("R-Type")
	resource['ground'] = SDL_SetVideoMode(1, 1, 32, SDL_HWSURFACE)
	SDL_FillRect(resource['ground'], nil, SDL_MapRGB(resource['ground'], 0x000000))

	resource['tile_map'] = IMG_Load(__DIR__ .. "/" .. game .. "/tile_map.png")
	SDL_SetColorKey(resource['tile_map'], SDL_SRCCOLORKEY, SDL_MapRGB(resource['tile_map'], 0xFF0000))

	resource.tile = {}
	resource.tile['basic'] = {resource['tile_map'], coordTile(0, 0), 0, {}}
	resource.tile['wall'] = {resource['tile_map'], coordTile(1, 0), 0, {}}
end

function init_player()
	player = {name = "Hunter", 2 * resource['head']['sizeTile'], 4 * resource['head']['sizeTile'], move = {false, false, false, false}, state = {}, collisionLenience = {0, 10}}
	resource['player_map'] = IMG_Load(__DIR__ .. "/" .. game .. "/player_map.png")
	SDL_SetColorKey(resource['player_map'], SDL_SRCCOLORKEY, SDL_MapRGB(resource['player_map'], 0x012f45))

	resource.player = {}
	resource.player['rightProfileNormal'] = {resource['player_map'], {54, 0}, 0, {}}
	resource.player['rightProfileDescent1'] = {resource['player_map'], {26, 0}, 0, {}}
	resource.player['rightProfileDescent2'] = {resource['player_map'], {0, 0}, 0, {}}
	resource.player['rightProfileRise1'] = {resource['player_map'], {82, 0}, 0, {}}
	resource.player['rightProfileRise2'] = {resource['player_map'], {110, 0}, 0, {}}
	player.state['spriteN'] = ''
	player.state['curSprite'] = 'rightProfileNormal'
end

function player_inter(event)
	if event.type == SDL_KEYDOWN then
		if event.key.keysym.sym == SDLK_DOWN then
			player.move[1] = true
		elseif event.key.keysym.sym == SDLK_UP then
			player.move[2] = true
		elseif event.key.keysym.sym == SDLK_LEFT then
			player.move[3] = true
		elseif event.key.keysym.sym == SDLK_RIGHT then
			player.move[4] = true
		end
	elseif event.type == SDL_KEYUP then
		if event.key.keysym.sym == SDLK_DOWN then
			player.move[1] = false
		elseif event.key.keysym.sym == SDLK_UP then
			player.move[2] = false
		elseif event.key.keysym.sym == SDLK_LEFT then
			player.move[3] = false
		elseif event.key.keysym.sym == SDLK_RIGHT then
			player.move[4] = false
		end
	end
	if player.move[1] or player.move[2] then
		if player.state['spriteN'] == '' then
			player.state['spriteN'] = '1'
		else
			player.state['spriteN'] = '2'
		end
	else
		player.state['spriteN'] = ''
		player.state['curSprite'] = 'rightProfileNormal'
	end

	if player.move[3] then
		outflow("START3\t" .. player[1] .. "\n")
		if testCollision(player[1] - 2, player[2]) then
			player[1] = player[1] - 2
		end
		outflow("END3\t" .. player[1] .. "\n")
	end
	if player.move[4] then
		outflow("START4\t" .. player[1] .. "\n")
		if testCollision(player[1] + 2, player[2]) then
			player[1] = player[1] + 2
		end
		outflow("END4\t" .. player[1] .. "\n")
	end
	if player.move[1] then
		if testCollision(player[1], player[2] + 2) then
			player[2] = player[2] + 2
		end
		player.state['curSprite'] = 'rightProfileDescent'
	end
	if player.move[2] then
		if testCollision(player[1], player[2] - 2) then
			player[2] = player[2] - 2
		end
		player.state['curSprite'] = 'rightProfileRise'
	end
	local ix = 0
	local iy = 0
	if debugMod == true then
		ix = (player[1] - player[1] % resource['head']['sizeTile']) / resource['head']['sizeTile'] + 1
		iy = (player[2] - player[2] % resource['head']['sizeTile']) / resource['head']['sizeTile'] + 1
	end
	SDL_BlitSurface(resource.player[player.state['curSprite'] .. player.state['spriteN']][1], {resource.player[player.state['curSprite'] .. player.state['spriteN']][2][1], resource.player[player.state['curSprite'] .. player.state['spriteN']][2][2], 27}, resource['ground'], {player[1] + ix, player[2] + iy})
	SDL_Delay(80)
end

resource['map'] =	{
			{{name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'basic', collision = true}, {name = 'wall', collision = false}},
			{{name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}, {name = 'wall', collision = false}},
		}
