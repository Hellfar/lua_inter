function init_map()
	resource['head']['sizeTile'] = 16
	icone = IMG_Load(__DIR__ .. "/Zelda/Zelda_ico.png")
	SDL_SetColorKey(icone, SDL_SRCCOLORKEY, SDL_MapRGB(icone, 0x000000))
	SDL_WM_SetIcon(icone)
	SDL_WM_SetCaption("Zelda")
	resource['ground'] = SDL_SetVideoMode(1, 1, 32, SDL_HWSURFACE)
	SDL_FillRect(resource['ground'], nil, SDL_MapRGB(resource['ground'], 0x000000))

	resource['tile_map'] = IMG_Load(__DIR__ .. "/Zelda/la_overtile.png")
	SDL_SetColorKey(resource['tile_map'], SDL_SRCCOLORKEY, SDL_MapRGB(resource['tile_map'], 0x004080))

	resource.tile = {}
	resource.tile['grass'] = {resource['tile_map'], coordTile(1, 6), 0, {}}
	resource.tile['grassCornerTopLeft'] = {resource['tile_map'], coordTile(0, 5), 0, {}}
	resource.tile['grassCenterTop'] = {resource['tile_map'], coordTile(1, 5), 0, {}}
	resource.tile['grassCornerTopRight'] = {resource['tile_map'], coordTile(2, 5), 0, {}}
	resource.tile['grassCenterLeft'] = {resource['tile_map'], coordTile(0, 6), 0, {}}
	resource.tile['grassCenterRight'] = {resource['tile_map'], coordTile(2, 6), 0, {}}
	resource.tile['grassCornerBottomLeft'] = {resource['tile_map'], coordTile(0, 7), 0, {}}
	resource.tile['grassCenterBottom'] = {resource['tile_map'], coordTile(1, 7), 0, {}}
	resource.tile['grassCornerBottomRight'] = {resource['tile_map'], coordTile(2, 7), 0, {}}
	resource.tile['lowGrass'] = {resource['tile_map'], coordTile(4, 9), 0, {}}
	resource.tile['bush'] = {resource['tile_map'], coordTile(13, 10), 0, {}}
	resource.tile['sand'] = {resource['tile_map'], coordTile(6, 10), 0, {}}
	resource.tile['roofCornerLeftTopBlue'] = {resource['tile_map'], coordTile(0, 0), 0, {}}
	resource.tile['roofCornerLeftBottomBlue'] = {resource['tile_map'], coordTile(0, 1), 0, {}}
	resource.tile['roofCornerRightTopBlue'] = {resource['tile_map'], coordTile(2, 0), 0, {}}
	resource.tile['roofCornerRightBottomBlue'] = {resource['tile_map'], coordTile(2, 1), 0, {}}
	resource.tile['roofCenterTopBlue'] = {resource['tile_map'], coordTile(1, 0), 0, {}}
	resource.tile['roofCenterBottomBlue'] = {resource['tile_map'], coordTile(1, 1), 0, {}}
	resource.tile['windowGrey'] = {resource['tile_map'], coordTile(0, 2), 0, {}}
	resource.tile['doorGrey'] = {resource['tile_map'], coordTile(2, 2), 0, {}}
	resource.tile['trunkLeft'] = {resource['tile_map'], coordTile(8, 8), 0, {}}
	resource.tile['trunkRight'] = {resource['tile_map'], coordTile(9, 8), 0, {}}
	resource.tile['treeCornerLeft'] = {resource['tile_map'], coordTile(8, 7), 0, {}}
	resource.tile['treeCornerRight'] = {resource['tile_map'], coordTile(9, 7), 0, {}}
	resource.tile['treeWoodCornerLeft'] = {resource['tile_map'], coordTile(8, 6), 0, {}}
	resource.tile['treeWoodCornerRight'] = {resource['tile_map'], coordTile(9, 6), 0, {}}
end

function init_player()
	player = {name = "Link", 2 * resource['head']['sizeTile'], 4 * resource['head']['sizeTile'], move = {false, false, false, false}, state = {}, collisionLenience = {0, 10}}
	resource['player_map'] = IMG_Load(__DIR__ .. "/Zelda/la_link.png")
	SDL_SetColorKey(resource['player_map'], SDL_SRCCOLORKEY, SDL_MapRGB(resource['player_map'], 0xFFFFFF))

	resource.player = {}
	resource.player['face1'] = {resource['player_map'], {0, 0}, 0, {}}
	resource.player['face2'] = {resource['player_map'], {18, 0}, 0, {}}
	resource.player['back1'] = {resource['player_map'], {0, 17}, 0, {}}
	resource.player['back2'] = {resource['player_map'], {18, 17}, 0, {}}
	resource.player['profileL1'] = {resource['player_map'], {35, 0}, 0, {}}
	resource.player['profileL2'] = {resource['player_map'], {52, 0}, 0, {}}
	resource.player['profileR1'] = {resource['player_map'], {35, 17}, 0, {}}
	resource.player['profileR2'] = {resource['player_map'], {52, 17}, 0, {}}

	player.state['spriteN'] = '1'
	player.state['curSprite'] = 'face'
end

function player_inter(event)
	if event.type == SDL_KEYDOWN then
		if event.key.keysym.sym == SDLK_DOWN then
			player.move[1] = true
		elseif event.key.keysym.sym == SDLK_UP then
			player.move[2] = true
		elseif event.key.keysym.sym == SDLK_LEFT then
			player.move[3] = true
		elseif event.key.keysym.sym == SDLK_RIGHT then
			player.move[4] = true
		end
	elseif event.type == SDL_KEYUP then
		if event.key.keysym.sym == SDLK_DOWN then
			player.move[1] = false
		elseif event.key.keysym.sym == SDLK_UP then
			player.move[2] = false
		elseif event.key.keysym.sym == SDLK_LEFT then
			player.move[3] = false
		elseif event.key.keysym.sym == SDLK_RIGHT then
			player.move[4] = false
		end
	end
	if player.move[1] or player.move[2] or player.move[3] or player.move[4] then
		if player.state['spriteN'] == '1' then
			player.state['spriteN'] = '2'
		else
			player.state['spriteN'] = '1'
		end
	end
	if player.move[3] then
		if testCollision(player[1] - 2, player[2]) then
			player[1] = player[1] - 2
		end
		player.state['curSprite'] = 'profileL'
	end
	if player.move[4] then
		if testCollision(player[1] + 2, player[2]) then
			player[1] = player[1] + 2
		end
		player.state['curSprite'] = 'profileR'
	end
	if player.move[1] then
		if testCollision(player[1], player[2] + 2) then
			player[2] = player[2] + 2
		end
		player.state['curSprite'] = 'face'
	end
	if player.move[2] then
		if testCollision(player[1], player[2] - 2) then
			player[2] = player[2] - 2
		end
		player.state['curSprite'] = 'back'
	end
	local ix = 0
	local iy = 0
	if debugMod == true then
		ix = (player[1] - player[1] % resource['head']['sizeTile']) / resource['head']['sizeTile'] + 1
		iy = (player[2] - player[2] % resource['head']['sizeTile']) / resource['head']['sizeTile'] + 1
	end
	SDL_BlitSurface(resource.player[player.state['curSprite'] .. player.state['spriteN']][1], {resource.player[player.state['curSprite'] .. player.state['spriteN']][2][1], resource.player[player.state['curSprite'] .. player.state['spriteN']][2][2], resource['head']['sizeTile']}, resource['ground'], {player[1] + ix, player[2] + iy})
	SDL_Delay(80)
end

resource['map'] =	{
			{{name = 'trunkLeft', collision = false}, {name = 'trunkRight', collision = false}, {name = 'trunkLeft', collision = false}, {name = 'trunkRight', collision = false}, {name = 'trunkLeft', collision = false}, {name = 'trunkRight', collision = false}, {name = 'trunkLeft', collision = false}, {name = 'trunkRight', collision = false}},
			{{name = 'treeCornerRight', collision = false}, {name = 'roofCornerLeftTopBlue', collision = false}, {name = 'roofCenterTopBlue', collision = false}, {name = 'roofCornerRightTopBlue', collision = false}, {name = 'bush', collision = false}, {name = 'bush', collision = false}, {name = 'bush', collision = false}, {name = 'bush', collision = false}},
			{{name = 'trunkRight', collision = false}, {name = 'roofCornerLeftBottomBlue', collision = false}, {name = 'roofCenterBottomBlue', collision = false}, {name = 'roofCornerRightBottomBlue', collision = false}, {name = 'bush', collision = false}, {name = 'lowGrass', collision = true}, {name = 'lowGrass', collision = true}, {name = 'bush', collision = false}},
			{{name = 'treeCornerRight', collision = false}, {name = 'windowGrey', collision = false}, {name = 'doorGrey', collision = true}, {name = 'windowGrey', collision = false}, {name = 'bush', collision = false}, {name = 'lowGrass', collision = true}, {name = 'lowGrass', collision = true}, {name = 'bush', collision = false}},
			{{name = 'trunkRight', collision = false}, {name = 'grassCornerTopLeft', collision = true}, {name = 'grassCenterTop', collision = true}, {name = 'grassCornerTopRight', collision = true}, {name = 'bush', collision = false}, {name = 'lowGrass', collision = true}, {name = 'lowGrass', collision = true}, {name = 'bush', collision = false}},
			{{name = 'treeCornerRight', collision = false}, {name = 'grassCenterLeft', collision = true}, {name = 'grass', collision = true}, {name = 'grassCenterRight', collision = true}, {name = 'bush', collision = false}, {name = 'bush', collision = false}, {name = 'bush', collision = false}, {name = 'bush', collision = false}},
			{{name = 'trunkRight', collision = false}, {name = 'grassCenterLeft', collision = true}, {name = 'grass', collision = true}, {name = 'grass', collision = true}, {name = 'grassCenterTop', collision = true}, {name = 'grassCenterTop', collision = true}, {name = 'grassCenterTop', collision = true}, {name = 'grassCenterTop', collision = true}},
			{{name = 'treeCornerRight', collision = false}, {name = 'grassCornerBottomLeft', collision = true}, {name = 'grassCenterBottom', collision = true}, {name = 'grassCenterBottom', collision = true}, {name = 'grassCenterBottom', collision = true}, {name = 'grassCenterBottom', collision = true}, {name = 'grassCenterBottom', collision = true}, {name = 'grassCenterBottom', collision = true}},
			{{name = 'treeWoodCornerLeft', collision = false}, {name = 'treeCornerRight', collision = false}, {name = 'treeCornerLeft', collision = false}, {name = 'treeCornerRight', collision = false}, {name = 'treeCornerLeft', collision = false}, {name = 'treeCornerRight', collision = false}, {name = 'treeCornerLeft', collision = false}, {name = 'treeCornerRight', collision = false}}
		}
