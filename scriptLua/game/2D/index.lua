ProcessMod = true

resource =	{
				head =	{
							height = 0,
							width = 0,
							sizeTile = 0
						},
				ground = nil,
				map = nil
			}

allowConsole(true)
outflow("Please enter your game : ")
game = incflow()
include(__DIR__ .. "/" .. game .. "/map.lua")
allowConsole(false)

function main()
  -- For Windows
	-- sdllib = load_lib(__DIR__ .. "/" .. game .. "/SDL_Lua_lib.dll")
  -- For Linux
	sdllib = load_lib(__DIR__ .. "/" .. game .. "/libSDL_Lua.so")

	if sdllib == nil then
		raise("SDLlib\n")
	end
	init_map()
	init_player()
	trace()
	unload_lib(sdllib)
end

function coordTile(x, y)
	return {resource['head']['sizeTile'] * x + (x + 1), resource['head']['sizeTile'] * y + (y + 1)}
end

function putTile(name, x, y)
	local sizeTile = resource['head']['sizeTile']
	local b = false
	if x > resource['head']['width'] then
		resource['head']['width'] = x
		b = true
	end
	if y > resource['head']['height'] then
		resource['head']['height'] = y
		b = true
	end
	if b == true then
		resource['ground'] = SDL_SetVideoMode(resource['head']['width'] * sizeTile, resource['head']['height'] * sizeTile, 32, SDL_HWSURFACE)
	end
	if resource.tile[name] ~= nil then
		if debugMod == true then
			SDL_BlitSurface(resource.tile[name][1], {resource.tile[name][2][1], resource.tile[name][2][2], sizeTile}, resource['ground'], {(x - 1) * sizeTile + x, (y - 1) * sizeTile + y})
		else
			SDL_BlitSurface(resource.tile[name][1], {resource.tile[name][2][1], resource.tile[name][2][2], sizeTile}, resource['ground'], {(x - 1) * sizeTile, (y - 1) * sizeTile})
		end
	end
end

function testCollision(px, py)
	local sizeTile = resource['head']['sizeTile']
	local rx = px % sizeTile / sizeTile
	local ry = py % sizeTile / sizeTile
	local b = resource['map'][py / sizeTile - ry + 1][px / sizeTile - rx + 1]['collision']
	if rx > 0 and b == true then
		b = resource['map'][py / sizeTile - ry + 1][px / sizeTile - rx + 2]['collision']
	end
	if ry > 0 and b == true then
		b = resource['map'][py / sizeTile - ry + 2][px / sizeTile - rx + 1]['collision']
	end
	return b
end

function trace()
	local sizeTile = resource['head']['sizeTile']
	local y, x
	debugMod = false
	while true do
		y = 1
		while y <= #resource['map'] do
			x = 1
			while x <= #resource['map'][y] do
				putTile(resource['map'][y][x]['name'], x, y, debugMod)
				x = x + 1
			end
			y = y + 1
		end
        event = SDL_PollEvent()
		if event.type == SDL_KEYDOWN then
			if event.key.keysym.sym == SDLK_ESCAPE then
				return
			elseif event.key.keysym.sym == SDLK_d then
				if debugMod == true then
					debugMod = false
					resource['ground'] = SDL_SetVideoMode(resource['head']['width'] * sizeTile, resource['head']['height'] * sizeTile, 32, SDL_HWSURFACE)
					SDL_FillRect(resource['ground'], nil, SDL_MapRGB(resource['ground'], 0x000000))
				else
					debugMod = true
					resource['ground'] = SDL_SetVideoMode(resource['head']['width'] * sizeTile + x, resource['head']['height'] * sizeTile + y, 32, SDL_HWSURFACE)
					SDL_FillRect(resource['ground'], nil, SDL_MapRGB(resource['ground'], 0xFF0000))
				end
				allowConsole(debugMod)
			end
		end
		if event.type == SDL_QUIT then
			return
		end
		player_inter(event)
		SDL_Flip(resource['ground'])
		outflow(SDL_GetError())
	end
end
