-- allowConsole(true)
ProcessMod = true

function main()
	sdllib = load_lib("SDL_Lua_lib.dll")
	if sdllib ~= nil then
		width = 700
		height = 700
		ground = SDL_SetVideoMode(width, height, 32, SDL_HWSURFACE + SDL_RESIZABLE)
		trace(ground, width, height)
		unload_lib(sdllib)
	else
		outflow("\"SDL_Lua_lib.dll\" have not been found.\n")
	end
	outflow('Veuillez appuyer sur ENTREE pour continuer.\n')
	incflow()
end


function trace(ground, width, height)
	color = {0, 0, 0}
	i = 1
	
	while true do
        event = SDL_WaitEvent()
		if event.type == SDL_QUIT or event.type == SDL_KEYDOWN then
			return
		elseif event.type == SDL_VIDEORESIZE then
			width = event.resize.w
			height = event.resize.h
			ground = SDL_SetVideoMode(width, height, 32, SDL_HWSURFACE + SDL_RESIZABLE)
		end
		
		width_src = width - 255
		height_src = height - 255
		if width_src < 1 or height_src < 1 then
			return
		else
			srfc = {SDL_CreateRGBSurface(SDL_HWSURFACE, width_src, height_src, 32), SDL_CreateRGBSurface(SDL_HWSURFACE, width_src, height_src, 32)}
			SDL_FillRect(srfc[1], nil, SDL_MapRGB(ground, 255, 255, 255))
			SDL_FillRect(srfc[0], nil, SDL_MapRGB(ground, 0, 0, 0))
			
			if color[i] == 255 then
				if i == 3 then
					--color = {0, 0, 0}
					i = 0
				end
				i = i + 1
			end
			
			SDL_WM_SetCaption("first SDL prgm " .. event.type .. " R" .. color[1] .. " G" .. color[2] .. " B" .. color[3] .. " w" .. width .. " h" .. height)
			SDL_FillRect(ground, nil, SDL_MapRGB(ground, color[1], color[2], color[3]))
			SDL_BlitSurface(srfc[1], nil, ground, color[1])
			SDL_BlitSurface(srfc[2], nil, ground, color[2])
			SDL_Flip(ground)
			
			color[i] = color[i] + 1
			if i == 1 then
				if color[3] > 0 then
					color[3] = color[3] - 1
				end
			else
				color[i - 1] = color[i - 1] - 1
			end
			
			SDL_FreeSurface(srfc[1])
			SDL_FreeSurface(srfc[2])
		end
	end
end