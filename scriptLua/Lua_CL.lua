allowConsole(true)
setTitleConsole("Lua CL")
-- std_lua()
-- os.execute("color 07")
-- os.execute("Cls")

context = {{"=> ", ":> "}, {true, false}}
o = 1
r = true
cut = ''
dothis = true

while r do
	--[[
	if context[2][o] == true then
		outflow("true\n")
	else
		outflow("false\n")
	end
	--]]
	
	outflow(context[1][o])
	cmd = incflow()
	if cmd == "bs" then
		if o == 1 then
			o = 2
		else
			o = 1
		end
		dothis = false
	elseif cmd == "exit" or cmd == "quit" or cmd == "q" then
		r = false
		dothis = false
	elseif cmd == "cut" then
		if cut == '' and dothis == true then
			dothis = false
		else
			dothis = true
			cmd = ''
		end
	else
		-- if #cmd >= 3 and cmd[#cmd - 2] == ">>>" then
			-- dothis = false
		-- else
			-- dothis = true
		-- end
	end
	
	if dothis == true then
		if eval(cut .. cmd, context[2][o]) == false then
			outflow(__ERROR__ .. "\n")
		end
		cut = ''
	else
		if cut == "\ncut" then
			cut = cmd
		else
			cut = cut .. "\n" .. cmd
		end
	end
end