@echo OFF
Cls

rem associer une extension a un programme

Set /p ext=Extension a associer (sans le point) ?
Set /p path=chemin racine du programme a executer (sans l'antislash a la fin) ?
Set /p prgm=Programme a executer (sera executer en tant que commande) ?
Set /p ID=ID du programme pour le registre (un mot simple) ?
Set /p icon=L'icon a apliquer au fichier (chemin complet) ?
Set /p desc=La dexcription a associer au fichier (descritpion en infoBull) ?

reg query HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.%ext%\OpenWithList /V "a"
reg add HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.%ext%\OpenWithList /V "a" /T "REG_SZ" /D "%path%\%prgm%"
reg add HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.%ext%\OpenWithList /V "MRUList" /T "REG_SZ" /D "a"

reg query HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.%ext%\OpenWithProgids /V "%prgm%.%ext%"
reg add HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.%ext%\OpenWithProgids /V "%prgm%.%ext%" /T "REG_NONE" /D ""

reg query HKEY_CLASSES_ROOT\.%ext% /V ""
reg add HKEY_CLASSES_ROOT\.%ext% /V "" /T "REG_SZ" /D "%ID%.%ext%"

reg query HKEY_CLASSES_ROOT\.%ext% /V "PerceivedType"
reg add HKEY_CLASSES_ROOT\.%ext% /V "PerceivedType" /T "REG_SZ" /D "text"

reg query HKEY_CLASSES_ROOT\%ID%.%ext% /V ""
reg add HKEY_CLASSES_ROOT\%ID%.%ext% /V "" /T "REG_SZ" /D "%desc%"

reg query HKEY_CLASSES_ROOT\%ID%.%ext%\DefaultIcon /V ""
reg add HKEY_CLASSES_ROOT\%ID%.%ext%\DefaultIcon /V "" /T "REG_SZ" /D "%icon%"

reg query HKEY_CLASSES_ROOT\%ID%.%ext%\shell /V ""
reg add HKEY_CLASSES_ROOT\%ID%.%ext%\shell /V "" /T "REG_SZ" /D "Open"

PAUSE