# Lua_inter

An old project intended to be a Lua sandbox.

## Description

This is a very very old program of my own, I made it before university so please don't judge, be kind :) .  
By versionning this project I just wanted to prevent it from being lost.  
Among all of the versions that I have made, this one is the one that was intended to be cross platform.
Also, please note that I was tinkering on it at the time and never really tried to finish it, so it may appears uncomplete from a lot of perspective.

## License

Some of the content may not belong to me.
For example: the sprites for R-Type, Tetris and Zelda belong to their owners respectively.