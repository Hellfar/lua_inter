#include	<math.h>
#include	<sstream>
#include	<string>
#include	<lua/lua.hpp>

#include	"LuaVar.hh"

// INITIALISING
LuaVar::LuaVar()
{
	this->putVal();
}

LuaVar::LuaVar(bool val)
{
	this->putVal(val);
}

LuaVar::LuaVar(int val)
{
	this->putVal(val);
}

LuaVar::LuaVar(float val)
{
	this->putVal(val);
}

LuaVar::LuaVar(std::string val)
{
	this->putVal(val);
}

LuaVar::LuaVar(lua_State *state, string nameVar)
{
	this->getLuaVar(state, nameVar);
}

// DEFINE
/*void					LuaVar::putLuaVar(lua_State *state, std::string nameVar)
{
	if (this->typeVal == "nil")
	{
		lua_pushnil(oprgm_data->state_index);
	}
	else if (this->typeVal == "boolean")
	{
	
	}
	else if (this->typeVal == "integer")
	{
	
	}
	else if (this->typeVal == "float")
	{
	
	}
	else if (this->typeVal == "string")
	{
	
	}
	lua_setglobal(oprgm_data->state_index, (char *)nameVar.c_str());
	
	//luaL_dostring(state, addVar.c_str());
}*/

void					LuaVar::putVal()
{
	this->typeVal = "nil";
	this->activeVal = false;
	
	this->boolVal = false;
	this->integerVal = 0;
	this->floatVal = 0;
	this->stringVal = "";
}

void					LuaVar::putVal(bool val)
{
	this->typeVal = "boolean";
	this->activeVal = true;
	
	this->boolVal = val;
	this->integerVal = val;
	this->floatVal = val;
	
	if (val)
	{
		this->stringVal = "TRUE";
	}
	else
	{
		this->stringVal = "FALSE";
	}
}

void					LuaVar::putVal(int val)
{
	std::ostringstream		buffer;
	buffer << val;
	
	this->typeVal = "integer";
	this->activeVal = true;
	
	if (val != 0)
	{
		this->boolVal = true;
	}
	else
	{
		this->boolVal = false;
	}
	
	this->integerVal = val;
	this->floatVal = val;
	this->stringVal = buffer.str();
}

void					LuaVar::putVal(float val)
{
	std::ostringstream buffer;
	buffer << val;
	
	this->typeVal = "float";
	this->activeVal = true;
	
	if (val != 0)
	{
		this->boolVal = true;
	}
	else
	{
		this->boolVal = false;
	}
	this->integerVal = ::floor(val);
	this->floatVal = val;
	this->stringVal = buffer.str();
}

void					LuaVar::putVal(string val)
{
	std::istringstream buffer(val);
	
	this->typeVal = "string";
	this->activeVal = true;
	
	this->boolVal = true;
	this->buffer >> integerVal;
	buffer >> floatVal;
	this->stringVal = val;
}

void					LuaVar::refresh()
{
	// finir
}

// GET
void					LuaVar::getLuaVar(lua_State *state, string nameVar)
{
	bool temp_bool;
	int temp_int;
	float temp_float;
	string temp_string;
	
	//lua_settop(state, 0);
	lua_getglobal(state, nameVar.c_str()); // select var
	
	this->name = nameVar.c_str();
	
	if (lua_isnil(state, 1))
	{
		this->putVal();
	}
	else if (lua_isboolean(state, 1))
	{
		temp_bool = (bool) lua_toboolean(state, 1);
	
		this->putVal(temp_bool);
	}
	else if (lua_isnumber(state, 1))
	{
		temp_float = (float) lua_tonumber(state, 1);
		temp_int = ::floor(temp_float);
	
		if (temp_float == temp_int)
		{
			this->putVal(temp_int);
		}
		else
		{
			this->putVal(temp_float);
		}
	}
	else if (lua_isstring(state, 1))
	{
		temp_string = (std::string) lua_tostring(state, 1);
	
		this->putVal(temp_string);
	}
	else
	{
		this->putVal();
	}
	
	lua_pop(state, 1);
}

std::string				LuaVar::getType()
{
	return (typeVal.c_str());
}

bool					LuaVar::isReadable()
{
	return (activeVal);
}

bool					LuaVar::getBool()
{
	return (boolVal);
}

int						LuaVar::getInt()
{
	return (integerVal);
}

float					LuaVar::getFloat()
{
	return (floatVal);
}

std::string				LuaVar::getString()
{
	return (stringVal);
}
