#include	"unistd.h"
#include	<cstdlib>
#include	<utility>
#include	<queue>
#include	<vector>
#include	<sstream>
#include	<iostream>
#include	<lua/lua.hpp>
#include	"usual/printLog.hh"
#include	"usual/std_fun.hh"
#include	"LuaVar.hh"
#include	"I_Console.hh"
#include	"Console.hh"
#include	"funState/register.hh"

#include	"I_Lua_inter.hh"
#include	"Lua_inter.hh"

std::map< lua_State *, I_Lua_inter * >	convLink;

Lua_inter::Lua_inter( std::string const & _argsControl )
	:state(luaL_newstate())
	,argsControl(_argsControl)
	,stateSTDLUALib(false)
	,currentFile("")
	,console(NULL)
{
	char *				buffcwd;
	std::ostringstream	temp_argsControl;

	temp_argsControl << "argsControl=" << this->argsControl << DEFAULT_PRGM_VARNAME << "=" << this << std::endl;

	buffcwd = getcwd(NULL, 0);
	this->currentDir = buffcwd;
	free(buffcwd);

	convLink[this->state] = this;

	registerToLua(this->state);

#ifdef	__INFO
	std::clog << std::log << "GLOBAL_VAR :" << std::endl << temp_argsControl.str() << " ;" << std::endl;
#endif

	if (luaL_dostring(this->state, temp_argsControl.str().c_str()))
		std::clog << std::error << "Error occur in the string : " << temp_argsControl.str() << " ;" << std::endl;
}

Lua_inter::Lua_inter( Lua_inter const & other )
	:state(luaL_newstate())
	,argsControl(other.argsControl)
	,args(other.args)
	,stateSTDLUALib(false)
	,currentDir(other.currentDir)
	,currentFile("")
	,console(NULL)
{
	std::ostringstream	temp_argsControl;

	temp_argsControl << "argsControl=" << this->argsControl << DEFAULT_PRGM_VARNAME << "=" << this << std::endl;

	convLink[this->state] = this;

	registerToLua(this->state);

#ifdef	__INFO
	std::clog << std::log << "GLOBAL_VAR :" << std::endl << temp_argsControl.str() << ";" << std::endl;
#endif

	if (luaL_dostring(this->state, temp_argsControl.str().c_str()))
		std::clog << std::error << "Error occur in the string : " << temp_argsControl.str() << " ;" << std::endl;

	if (other.stateSTDLUALib == true)
		this->load_stdLibs();
	if (other.console != NULL)
		this->setConsole(true);

	// finir sur les libs
}

Lua_inter::~Lua_inter()
{
	this->setConsole(false);
	this->clearALibRef();
	convLink.erase(this->state);
	lua_close(this->state);
}

Lua_inter &				Lua_inter::operator=( Lua_inter const & other )
{
	std::ostringstream	temp_argsControl;

	temp_argsControl << other.argsControl;
	temp_argsControl << DEFAULT_PRGM_VARNAME << "=" << this << std::endl;
	this->argsControl = other.argsControl;
	if (other.stateSTDLUALib == true)
		this->load_stdLibs();
	//this->aLibRef = other.aLibRef; // finir sur les libs
	luaL_dostring(this->state, (this->argsControl.c_str()));

	return (*this);
}

bool					Lua_inter::dofl( std::string const & file )
{
	bool				error = false;
	std::string			fileToRun = solve_path(this->currentDir, file);
	std::string			backDir;
	std::string			backFile;
	//~ std::cout << file << " : " << fileToRun << std::endl;
	this->history.push(std::make_pair(std::make_pair(DOFILE, fileToRun), std::queue< std::string >()));
	if (is_readable(fileToRun) == true && path_parse(fileToRun, "extension") == "lua")
	{
		backDir = this->currentDir;
		this->currentDir = path_parse(fileToRun, "path_directory");
		lua_pushstring(this->state, this->currentDir.c_str());
		lua_setglobal(this->state, "__DIR__");
		backFile = this->currentFile;
		this->currentFile = path_parse(fileToRun, "file");
		lua_pushstring(this->state, this->currentFile.c_str());
		lua_setglobal(this->state, "__FILE__");

#ifdef	__INFO
		std::clog << std::log << "DO_FILE_LUA : " << fileToRun << " ;" << std::endl;
#endif

		if (luaL_dofile(this->state, fileToRun.c_str()))
		{
			std::clog << std::error << "SCRIPT : " << lua_tostring(this->state, -1) << std::endl;

			error = true;
		}
		else
		{
			lua_getglobal(this->state, DEFAULT_RETURN_VARNAME);
			if (lua_isboolean(this->state, 1) && lua_toboolean(this->state, 1))
			{
				lua_getglobal(this->state, DEFAULT_MAIN_FUNCTION);
				if (!lua_isfunction(this->state, -1))
				{
					std::clog << std::error << "SCRIPT :" << std::endl << "Function \"" DEFAULT_MAIN_FUNCTION "\" don't exist in " << fileToRun << " ;" << std::endl;

					lua_pop(this->state, 1);

					error = true;
				}
				else
				{
					std::ostringstream	ss_ret;
					int					ret;

					lua_call(this->state, 0, 1);
					ret = static_cast< int >(lua_tonumber(this->state, -1));
					ss_ret << ret;
					this->history.back().second.push(ss_ret.str());

#ifdef	__INFO
					std::clog << std::log << "RETURN PROCESS :" << std::endl << "Script \"" << fileToRun << "\" return \"" << ss_ret << "\" ;" << std::endl;
#endif

				}
			}
		}
		this->currentDir = backDir;
		if (backDir.length())
		{
			lua_pushstring(this->state, this->currentDir.c_str());
			lua_setglobal(this->state, "__DIR__");
		}
		else
			lua_unregister(this->state, "__DIR__");

		this->currentFile = backFile;
		if (backFile.length())
		{
			lua_pushstring(this->state, this->currentFile.c_str());
			lua_setglobal(this->state, "__FILE__");
		}
		else
			lua_unregister(this->state, "__FILE__");
	}
	else
	{
		std::clog << std::error << "File in dofl param is not a valid lua file : " << fileToRun << " ;" << std::endl;
		error = true;
	}

	if (error == true)
		this->history.back().second.push("-1");

	return (error);
}

bool					Lua_inter::dostr( std::string const & str )
{
	bool				error = false;

	this->history.push(std::make_pair(std::make_pair(DOSTRING, str), std::queue< std::string >()));
	if (luaL_dostring(this->state, str.c_str()))
		std::clog << std::error << "Error occur in the string : " << str << " ;" << std::endl;

	if (error == true)
		this->history.back().second.push("-1");

	return (error);
}

void					Lua_inter::load_stdLibs( void )
{
	if (!this->stateSTDLUALib)
	{
		::luaL_openlibs(this->state);
		this->stateSTDLUALib = true;
	}
}

lua_State *				Lua_inter::getState( void ) const
{
	return (this->state);
}

std::string const &		Lua_inter::getArgsControl( void ) const
{
	return (this->argsControl);
}

std::string const &		Lua_inter::getArgs( void ) const
{
	return (this->args);
}

std::queue< std::pair< std::pair< enum I_Lua_inter::e_typeEventHistory, std::string >, std::queue< std::string > > > const &	Lua_inter::getHistory( void ) const
{
	return (this->history);
}

int						Lua_inter::getReturn( void ) const
{
	std::istringstream	ss_ret;
	//~ std::string			s_ret(this->history.back().second.back());
	int					ret = EXIT_SUCCESS;

	if (this->history.size() > 0)
	{
		const std::queue< std::string > &	t_returnedInfos = this->history.back().second;
		if (t_returnedInfos.size() > 0)
		{
			ss_ret.str(t_returnedInfos.back());
			ss_ret >> ret;
		}
	}

	return (ret);
}

std::string const &		Lua_inter::getCurrentDir( void ) const
{
	return (this->currentDir);
}

std::string const &		Lua_inter::getCurrentFile( void ) const
{
	return (this->currentFile);
}

/*std::vector<TYPELIB> &	Lua_inter::getALibRef( void )
{
	return (this->aLibRef);
}*/

int						Lua_inter::addLibRef( std::string const & nameLib, std::string const & sparams )
{std::cout << "TEST0" << std::endl;
	type_LIB_init		cpp_initializer;
	TYPELIB				library = dlopen(solve_path(this->getCurrentDir(), nameLib).c_str(), RTLD_NOW);
	std::cout << "TEST1" << std::endl;
	if(library == NULL)
	{
		std::clog << std::error << "addLibRef : " << nameLib << " is unload (" << ::dlerror() << "). ;" << std::endl;

		return (-1);
	}
	else
	{
		this->aLibRef.push_back(library);

		cpp_initializer = (type_LIB_init)dlsym(library, DEFAULT_LIB_INIT);
    std::cout << cpp_initializer << std::endl;
		if(cpp_initializer == NULL)
		{
			std::clog << std::warning << std::endl << DEFAULT_LIB_INIT "() not found in " << nameLib << ". ;" << std::endl;
		}
		else
		{std::cout << "TEST2" << std::endl;
			cpp_initializer(this, sparams);
		}
	}
std::cout << "TEST3" << std::endl;
	return (this->aLibRef.size() - 1);
}

void					Lua_inter::eraseLibRef( unsigned int pos )
{
	type_LIB_init	cpp_uninitializer;

	if (pos < this->aLibRef.size() && this->aLibRef[pos] != NULL)
	{
		cpp_uninitializer = (type_LIB_init)dlsym(this->aLibRef[pos], DEFAULT_LIB_UNINIT);
		if(cpp_uninitializer == NULL)
		{
			std::clog << std::warning << std::endl << DEFAULT_LIB_UNINIT "() not found in lib position : " << pos << " ;" << std::endl;
		}
		else
		{
			cpp_uninitializer(this, "");
		}

		dlclose(this->aLibRef[pos]);
		this->aLibRef.erase(this->aLibRef.begin() + pos);
	}
}

void					Lua_inter::clearALibRef( void )
{
	while (this->aLibRef.size())
		this->eraseLibRef(0);
}

I_Console *				Lua_inter::getConsole( void ) const
{
	return (this->console);
}

void					Lua_inter::setConsole( bool state )
{
	if (state)
	{
		if (!this->console)
      std::cout << "setConsole1" << std::endl;
			this->console = new Console();
      std::cout << "setConsole2" << std::endl;
	}
	else
		if (this->console)
		{
			delete this->console;
			this->console = NULL;
		}
}
