/*
* mise en place de du system de chargement de bibliotheque dynamique, gestion des fichiers, gestions des process.
* migration sous VS2010 et debut de la comptatibilite avec g++.
* changement 
*/

#ifdef	_WIN32
# pragma	comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
//# ifdef _MSC_VER
//#  define _CRT_SECURE_NO_WARNINGS
//# endif
#endif

#include	"unistd_cross.h"
#include	"signal_cross.h"
#include	<clocale>
#include	<cerrno>
#include	<cstring>
# include	<cstdlib>
# include	<getopt.h> 

enum			e_OPTION	// setted by ...
{
	NONE		= 0,
	INPUTFLOW	= 1 << 0,
	UNISTATE 	= 1 << 1,
};

static struct option	long_options[] =
{
    { "args", required_argument, NULL, 'a' },
    { NULL, no_argument, NULL, '\0' },
};

#include	<exception>
#include	<string>
#include	<sstream>
#include	<fstream>
#include	<iostream>
#include	<lua/lua.hpp>
#include	"extend/ostream_extend.hpp"
#include	"usual/printLog.hh"
#include	"usual/std_fun.hh"
#include	"I_Lua_inter.hh"
#include	"Lua_inter.hh"

struct s_argsContainer {int * argc; char * * * argv;} ext_s_args;

int							main( int argc, char * * argv )
{
	int						ret;
	I_Lua_inter *			oprgm = NULL;
	std::ostringstream		argsControl;
	std::string				args;
	int						options = NONE;
	//~ FILE *				fb[2] = {stdin, stdout};
	
	setlocale(LC_ALL, "");
	
	ext_s_args.argc = &argc;
	ext_s_args.argv = &argv;
	
	try
	{
		if (::chdir(path_parse(argv[0], "path_directory").c_str()) == -1)
			throw (std::string() + "chdir error : " + strerror(errno) + ".");
		std::init_printLog();
		init_sigCatch();
		
		argsControl << "{" << argv << "}" << std::endl;
		oprgm = new Lua_inter(argsControl.str());
		
		{
			int			ch;
			int			option_index = 0;
			
			while ((ch = getopt_long(argc, argv, "a:cu", long_options, &option_index)) != -1)
				switch (ch)
				{
					case (0):
						if (optarg != NULL)
							oprgm->dofl(optarg);
						if ((options & UNISTATE) == UNISTATE)
						{
							delete (oprgm);
							oprgm = new Lua_inter(argsControl.str());
							if (args.length() != 0)
								oprgm->dostr(std::string() + "args=" + args + "");
						}
						break;
					case ('a'):
						args = optarg;
						oprgm->dostr(std::string() + "args=" + args + "");
						break;
					case ('c'):
						options |= INPUTFLOW;
						break;
					case ('u'):
						options |= UNISTATE;
						break;
				}
		};
		
		//~ argv += optind;
		
		while (optind < argc)
		{
			oprgm->dofl(argv[optind++]);
			if ((options & UNISTATE) == UNISTATE)
			{
				delete (oprgm);
				oprgm = new Lua_inter(argsControl.str());
				if (args.length() != 0)
					oprgm->dostr(std::string() + "args=" + args + "");
			}
		}
		
		if (oprgm->getHistory().size() == 0)
			oprgm->dofl();
		
		ret = oprgm->getReturn();
	}
	catch (int error_n)
	{
		ret = error_n;
		std::clog << std::error << "throw with \"" << error_n << "\" number." << std::endl;
	}
	catch (char * error_msg)
	{
		ret = -1;
		std::clog << std::error << error_msg << std::endl;
	}
	catch (std::exception & e)
	{
		ret = -1;
		std::clog << std::error << e.what() << std::endl;
	}
	catch (...)
	{
		ret = -1;
		std::clog << std::error << "Error encounter." << std::endl;
	}
	if (oprgm != NULL)
		delete (oprgm);
	
	std::init_printLog(NULL);
	
	return (ret);
}
