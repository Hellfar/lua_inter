#include	<lua/lua.hpp>
#include	"funState/funStateLua.hh"

#include	"funState/register.hh"

void				registerToLua( lua_State * L )
{
	{lua_pushnil(L); lua_setglobal(L, "NULL");} // define NULL const

	lua_register(L, "load_lib", &load_libFile);
	lua_register(L, "unload_lib", &unload_libFile);

	lua_register(L, "include", &include_luaFile);
	lua_register(L, "start", &start_lua);
	lua_register(L, "stop", &stop_lua);
	lua_register(L, "eval", &eval_lua);
	lua_register(L, "std_lua", &load_fun_lua);
	lua_register(L, "report", &report_lua);
	lua_register(L, "raise", &throw_lua);

	lua_register(L, "listDir", &listDir_lua);

	lua_register(L, "allowConsole", &allowConsole);
	lua_register(L, "getTitleConsole", &getTitleConsole);
	lua_register(L, "setTitleConsole", &setTitleConsole);
	lua_register(L, "setSectColCsl", &setSectColCsl);
	lua_register(L, "outflow", &outflow);
	lua_register(L, "incflow", &incflow);
	lua_register(L, "pause", &pause);
}
