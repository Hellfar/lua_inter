#include	<dlfcn.h>
#include	<fstream>
#include	<iostream>
#include	<sstream>
#include	<lua/lua.hpp>
#include	"funState/under_funStateLua.hh"
#include	"I_Lua_inter.hh"
#include	"Lua_inter.hh"
#include	"usual/std_fun.hh"
#include	"usual/printLog.hh"
#include	"conf.hh"

#include	"funState/funStateLua.hh"

//---------------------------------------------------including
int					load_libFile( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];
	std::string		libName;
	std::string		libParam;
	int				libRef;
	int				nbArguments = lua_gettop(L);

	if (!((nbArguments == 1 && lua_isstring(L, 1)) || (nbArguments == 2 && lua_isstring(L, 1) && lua_isstring(L, 2))))
	{
		std::clog << std::error << "loadlib : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;

		lua_pushnil(L);

		return (1);
	}

	libName = lua_tostring(L, 1);
	libParam = (nbArguments == 2) ? lua_tostring(L, 2) : "";

	libRef = oprgm->addLibRef(libName, libParam);

	if (libRef == -1)
		lua_pushnil(L);
	else
		lua_pushnumber(L, libRef);

	return (1);
}

int					unload_libFile( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];
	unsigned int	pos;
	int				nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isnumber(L, 1))
	{
		pos = static_cast< unsigned int >(lua_tonumber(L, 1));
		oprgm->eraseLibRef(pos);
	}
	else
	{
		std::clog << std::error << "unloadlib : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;
	}

	return (0);
}

int					include_luaFile( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];
	std::string		file;
	int				nbrArguments = lua_gettop(L);

	if (!((nbrArguments == 1 && lua_isstring(L, 1)) || (nbrArguments == 2 && (lua_isstring(L, 1) || lua_isboolean(L, 2) || lua_isnumber(L, 2)))))
	{
		std::clog << std::error << "include : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;

		return (0);
	}

	if (nbrArguments == 2 && !lua_toboolean(L, 2))
	{
		oprgm = new Lua_inter(*(static_cast< Lua_inter * >(oprgm)));
	}

	file = solve_path(oprgm->getCurrentDir(), lua_tostring(L, 1));

	oprgm->dofl(file);

	if (nbrArguments == 2 && !lua_toboolean(L, 2))
	{
		lua_pushnumber(L, oprgm->getReturn());
		delete oprgm;

		return (1);
	}

	return (0);
}
//---------------------------------------------------END

//---------------------------------------------------Console
int					allowConsole( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];
	bool			new_stateConsol = !oprgm->getConsole();
	int				nbArguments = lua_gettop(L);

	if (nbArguments == 1)
	{
		if (!lua_isboolean(L, 1))
		{
			std::clog << std::error << "allowConsol : " << INV_TYPE_ARG_MSGLOG << std::endl;
			return (0);
		}
		else
		{
			new_stateConsol = static_cast< bool >(lua_toboolean(L, 1));
		}
	}

	oprgm->setConsole(new_stateConsol);

	return (0);
}

int					getTitleConsole( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];
	int				nbArguments = lua_gettop(L);

	if (nbArguments != 0)
	{
		std::clog << std::error << "getTitleConsol : " << INV_NB_ARG_MSGLOG << std::endl;

		return (0);
	}

	if (oprgm->getConsole() != NULL)
	{
		lua_pushstring(L, oprgm->getConsole()->getTitle().c_str());
	}
	else
	{
		std::clog << std::warning << "getTitleConsol : " << CONSOLE_ABSENT << std::endl;
		lua_pushnil(L);
	}
	return (1);
}

int					setTitleConsole( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];
	std::string		temp_string;
	int				nbArguments = lua_gettop(L);

	if (oprgm->getConsole() != NULL)
	{
		if (nbArguments == 0)
		{
			temp_string = oprgm->getArgs();
		}
		else if (nbArguments == 1 && lua_isstring(L, 1))
		{
			temp_string = lua_tostring(L, 1);
		}
		else
		{
			std::clog << std::error << "setTitlteConsol : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;

			return (0);
		}
	}
	else
	{
		std::clog << std::warning << "setTitlteConsol : " << CONSOLE_ABSENT << std::endl;

		return (0);
	}

	if (oprgm->getConsole())
		oprgm->getConsole()->setTitle(temp_string);

	return (0);
}

int					setSectColCsl( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];
	int				nbArguments = lua_gettop(L);

	if (oprgm->getConsole() != NULL)
	{
		if (nbArguments == 0)
		{
			oprgm->getConsole()->setDefaultTextColor();
		}
		else if (nbArguments == 1 && lua_isnumber(L, 1))
		{
			oprgm->getConsole()->setDefaultTextColor((int)lua_tointeger(L, 1));
		}
		else
		{
			std::clog << std::error << "setSectColsCsl : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;
		}
	}
	else
	{
		std::clog << std::warning << "setSectColsCsl : " << CONSOLE_ABSENT << std::endl;
	}

	return (0);
}

int					outflow( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];
	int				nbArguments = lua_gettop(L);

	if (oprgm->getConsole() != NULL)
	{
		if (nbArguments == 1 && lua_isstring(L, 1))
		{
			if (oprgm->getConsole())
				oprgm->getConsole()->outflow(lua_tostring(L, 1));
		}
		else if (nbArguments == 2 && lua_isstring(L, 1) && lua_isnumber(L, 2))
		{
			if (oprgm->getConsole())
				oprgm->getConsole()->outflow(lua_tostring(L, 1), lua_tointeger(L, 2));
		}
		else
		{
			std::clog << std::error << "outflow : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;
		}
	}

	return (0);
}

int					incflow( lua_State *L )
{
	I_Lua_inter *	oprgm = convLink[L];
	int				nbArguments = lua_gettop(L);

	if (!(nbArguments == 0 || (nbArguments == 1 && lua_isnumber(L, 1))))
	{
		std::clog << std::error << "incflow : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;

		lua_pushnil(L);
		return (1);
	}

	if (oprgm->getConsole())
	{
		if (nbArguments == 1)
			lua_pushstring(L, (oprgm->getConsole()->incflow(static_cast< int >(lua_tonumber(L, 1)))).c_str());
		else
			lua_pushstring(L, (oprgm->getConsole()->incflow()).c_str());
	}

	return (1);
}

int					pause( lua_State *L )
{
	I_Lua_inter *	oprgm = convLink[L];
	int				nbArguments = lua_gettop(L);

	if (!(nbArguments == 0 || (nbArguments == 1 && lua_isstring(L, 1))))
	{
		std::clog << std::error << "incflow : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;
	}

	if (oprgm->getConsole())
	{
		if (nbArguments == 1)
			oprgm->getConsole()->outflow(lua_tostring(L, 1));
		oprgm->getConsole()->incflow(0);
	}
	else
		std::clog << std::warning << "pause : " << CONSOLE_ABSENT << std::endl;

	return (0);
}
//---------------------------------------------------END

//---------------------------------------------------lua
int					start_lua( lua_State * L )
{
	int				nbArguments = lua_gettop(L);

	if (nbArguments != 0)
	{
		std::clog << std::error << "start : " << INV_NB_ARG_MSGLOG << std::endl;

		lua_pushnil(L);
	}
	else
	{
		std::stringstream	t_ss;
		lua_Number	t_n;

		t_ss << (new Lua_inter());
		t_ss >> t_n;
		lua_pushnumber(L, t_n); // finir probable perte de la taille du pointeur
	}

	return (1);
}

int					stop_lua( lua_State * L )
{
	int				nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isnumber(L, 1))
		delete (reinterpret_cast< Lua_inter * >(static_cast< unsigned int >(lua_tonumber(L, 1)))); // finir pareil que pour start_lua
	else
	{
		std::clog << std::error << "stop : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;
	}
	return (0);
}

#if		defined(__gnuc__) && defined(_UNIX)

int					fork_lua( lua_State * L )
{
	int				nbArguments = lua_gettop(L);

	if (nbArguments != 0)
	{
		std::clog << std::warning << "fork : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;
	}

	lua_pushnumber(L, fork());

	return (1);
}

#endif

int					eval_lua( lua_State * L )
{
	I_Lua_inter *	oprgm;
	std::string		tmp_string;
	bool			tmp_bool = true;
	int				nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isstring(L, 1))
	{
		oprgm = convLink[L];
	}
	else if (nbArguments == 2 && lua_isstring(L, 1) && (lua_isboolean(L, 2) || lua_isnumber(L, 2)))
	{
		if (lua_isboolean(L, 2))
		{
			tmp_bool = static_cast< bool >(lua_toboolean(L, 2));
			oprgm = (tmp_bool) ? convLink[L] : (new Lua_inter());
		}
		else
		{
			oprgm = (reinterpret_cast< Lua_inter * >(static_cast< unsigned int >(lua_tonumber(L, 1))));
		}
	}
	else
	{
		std::clog << std::error << "eval : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;

		lua_pushnil(L);
		return (1);
	}

	tmp_string = lua_tostring(L, 1);

	if (luaL_dostring(oprgm->getState(), tmp_string.c_str()) != 0)
	{
		std::clog << std::error << "eval : " << lua_tostring(oprgm->getState(), -1) << std::endl;

		lua_pushstring(L, lua_tostring(oprgm->getState(), -1));
		lua_setglobal(L, "__ERROR__");

		lua_pushboolean(L, 0);
	}
	else
		lua_pushboolean(L, 1);

	if (!tmp_bool)
	{
		delete (oprgm);
	}

	return (1);
}

int					load_fun_lua( lua_State * L )
{
	I_Lua_inter *	oprgm = convLink[L];

	oprgm->load_stdLibs();

	return (0);
}

int					report_lua( lua_State * L )
{
	int				nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isstring(L, 1))
	{
		std::clog << lua_tostring(L, 1);
	}
	else
	{
		std::clog << std::warning << "report_lua : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;
	}

	return (0);
}

int					throw_lua( lua_State * L )
{
	int				nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isstring(L, 1))
	{
		char const *	t_s = lua_tostring(L, 1);
		std::clog << std::error << t_s;
		throw (t_s);
	}
	else
	{
		std::clog << "raise by lua without argument." << std::raise;
	}

	return (0);
}
//---------------------------------------------------END

//---------------------------------------------------talk
/*
communication entre programme.
*/
//---------------------------------------------------END

//---------------------------------------------------file
int					listDir_lua( lua_State *L )
{
	char *			path = (char *)".";
	bool			recurs = false;

	int nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isstring(L, 1))
	{
		path = (char *)lua_tostring(L, 1);
	}

	if (nbArguments >= 2 && lua_isboolean(L, 2))
	{
		recurs = static_cast< bool >(lua_toboolean(L, 2));
	}

	ur_listDir(L, path, recurs);

	return (1);
}
//---------------------------------------------------END

//---------------------------------------------------thread
/*
threads
*/
//---------------------------------------------------END

//---------------------------------------------------registre
/*int					getReg( lua_State * L )
{
	int				nbArguments = lua_gettop(L);
	if (nbArguments < 1 && nbArguments > 1)
	{
		fprintf(stderr,"getReg : nombre d'arguments invalide");
		return 0;
	}

	return (0);
}

int					setReg( lua_State * L )
{
	int				nbArguments = lua_gettop(L);
	if (nbArguments != 0)
	{
		fprintf(stderr,"setReg : nombre d'arguments invalide");
		return 0;
	}

	return (0);
}*/
//---------------------------------------------------END
