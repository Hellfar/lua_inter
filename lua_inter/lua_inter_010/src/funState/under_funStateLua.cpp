#ifdef	_WIN32
# include	<Windows.h>
#else
# include	<dirent.h>
#endif
#include	<cerrno>
#include	<cstdlib>
#include	<cstring>
#include	<fstream>
#include	<lua/lua.hpp>
#include	"usual/std_fun.hh"
#include	"I_Lua_inter.hh"

#include	"funState/under_funStateLua.hh"

void					ur_listDir( lua_State * L, std::string const & path, bool recurs )
{
	I_Lua_inter *		oprgm = convLink[L];
	std::string			solved = solve_path(oprgm->getCurrentDir(), path);
	unsigned int		i = 1;
#ifdef	_WIN32
	WIN32_FIND_DATA		FindFileData;
	HANDLE				rep;
	
	if ((rep = FindFirstFile((solved + "/*").c_str(), &FindFileData)) != INVALID_HANDLE_VALUE)
	{
#else
	DIR *				rep = NULL;
	struct dirent *		ent = NULL;

	if ((rep = opendir(solved.c_str())) != NULL)
	{
#endif

		lua_newtable(L);
		
		lua_pushstring(L, "d_name");
		lua_pushstring(L, path_parse(solved, "file").c_str());
		lua_settable(L, -3);
		
#ifdef	_WIN32
		do
		{
			if (::strcmp(FindFileData.cFileName, ".") && ::strcmp(FindFileData.cFileName, ".."))
			{
#else
		while ((ent = readdir(rep)) != NULL)
		{
			if (::strcmp(ent->d_name, ".") && ::strcmp(ent->d_name, ".."))
			{
#endif
				lua_pushnumber(L, i);

				if (recurs &&
#ifdef  _WIN32
					FindFileData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY
#else
					ent->d_type == DT_DIR
#endif
					)
				{
#ifdef	_WIN32
					ur_listDir(L, solved + "/" + FindFileData.cFileName, recurs);
#else
					ur_listDir(L, solved + "/" + ent->d_name, recurs);
#endif
				}
				else
				{
#ifdef	_WIN32
					lua_pushstring(L, FindFileData.cFileName);
#else
					lua_pushstring(L, ent->d_name);
#endif
				}
				lua_settable(L, -3);

				i++;
			}
		}
#ifdef	_WIN32
		while (FindNextFile(rep, &FindFileData));

		FindClose(rep);
#else

		closedir(rep);
#endif

	}
	else
	{
		lua_pushnil(L);
	}
}