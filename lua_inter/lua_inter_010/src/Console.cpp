#include	<cstdlib>
#include	<cstdio>
#include	<iostream>
#include	<string>
#ifdef	_WIN32
# include	<windows.h>
#else
# include	<pty.h>
#endif
#include	"usual/std_fun.hh"
#include	"usual/printLog.hh"

#include	"Console.hh"

Console::Console()
{
  #ifdef _WIN32
  	char				title[MAX_PATH];

  	AllocConsole();
  	freopen("CONOUT$", "wb", stdout);

  	this->hstdin  = GetStdHandle(STD_INPUT_HANDLE);
  	this->hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
  	this->hstderr = GetStdHandle(STD_ERR_HANDLE);

  	GetConsoleTitle(title, MAX_PATH);
  	this->title = title;
  	this->hwnd = FindWindow(NULL, this->title.c_str());
  #else
  	// int						master;
  	// int						slave;
  	// char					name[512];
  	// struct termios			termp;
  	// struct winsize			winp = (struct winsize){12, 12, 0, 0};
    //
  	// openpty(&master, &slave, name, &termp, &winp);
  	// std::cout << master << " " << slave << " " << name << " " << winp.ws_row << ":" << winp.ws_col << std::endl;
  #endif
}

Console::~Console()
{
	//~ FreeConsole();
}

std::string const &		Console::getTitle( void ) const
{
	return (this->title);
}

void					Console::setTitle( std::string const & title )
{
    this->title = title;

	//~ SetConsoleTitle(title.c_str());
}

void					Console::setDefaultTextColor( int color )
{
	//~ SetConsoleTextAttribute(this->hstdout, color);
}

void					Console::outflow( std::string const &str, int color ) const
{
	//~ CONSOLE_SCREEN_BUFFER_INFO	temp_csbi;

	if (color >= 0)
	{
		//std::cout << temp_csbi.wAttributes << std::endl;
		//~ GetConsoleScreenBufferInfo(this->hstdout, &temp_csbi);
		//std::cout << temp_csbi.wAttributes << std::endl;
		//~ SetConsoleTextAttribute(this->hstdout, color);
		//std::cout << temp_csbi.wAttributes << std::endl;
	}

	std::cout << str;

#ifdef __INFO
	//~ std::clog << std::log << "INFO (outflow()) :\n$temp_string = " << temp_string << "\n$temp_number = " << temp_number << "\n;\n";
#endif

	if (color >= 0)
	{
		//~ SetConsoleTextAttribute(this->hstdout, (int)(temp_csbi.wAttributes));
	}
}

std::string				Console::incflow( int max ) const
{
	std::streamsize		tmp;
	std::string			buff;

	//~ freopen("CONIN$", "a+", stdin);

	if (max > -1)
	{
		tmp = std::cin.width();
		std::cin.width(max);
	}

	if (max == 0)
		std::cin.get();
	else
		std::cin >> buff;

	if (max > -1)
		std::cin.width(tmp);

	//~ freopen("CONOUT$", "wb", stdout);

	return (buff);
}

// std::string const &		operator<<( Console const & inst, std::string const & str )
// {
	// inst.outflow(str);
	// return (str);
// }

// std::string const		operator>>( Console const & inst, std::string const & str )
// {
	// return(inst.incflow());
// }
