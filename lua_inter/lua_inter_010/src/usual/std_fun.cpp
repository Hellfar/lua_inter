#include	<sys/types.h>
#include	<sys/stat.h>
#include	<linux/limits.h>
#include	"unistd.h"
#include	<cstdlib>
#include	<sstream>
#include	<string>
#include	<cstring>
#include	<fstream>

#include	"usual/std_fun.hh"

#ifdef	_WIN32
# define	SEPARATOR_PATH	'\\'
#else
# define	SEPARATOR_PATH	'/'
#endif

void					clean_buffer( char * buf )
{
	char *				p = ::strchr (buf, '\n');
	
	if (p != NULL)
		*p = 0;
	else
	{
		int				c;
		
		while ((c = ::getchar ()) != '\n' && c != EOF)
			;
	}
}

std::string &			str_replace( const std::string & search, const std::string & replace, std::string & subject )
{
	std::string			buffer;
	
	int					sealeng = search.length();
	int					strleng = subject.length();
	
	if (sealeng == 0)
		return (subject);//no change
	
	for(int i = 0, j = 0; i < strleng; j = 0)
	{
		while (i + j < strleng && j < sealeng && subject[i + j] == search[j])
			j++;
		if (j == sealeng)//found 'search'
		{
			buffer.append(replace);
			i += sealeng;
		}
		else
		{
			buffer.append(&subject[i++], 1);
		}
	}
	
	subject = buffer;
	
	return (subject);
}

bool					is_readable( const std::string & _file ) // return true if file is readable, false else
{
	std::ifstream		file(_file.c_str());
	
	return (!file.fail());
}

std::string				solve_path(std::string const & l, std::string const & r)
{
	std::string			solved;
#ifdef	_WIN32
	char				separator =
#ifdef	_WIN32
									'\\'
#else
									'/'
#endif
										;
	size_t				posParent;
	
	if (
#ifdef	_WIN32
		isalpha(r[0]) && r[1] == ':'
#else
		r[0] == separator
#endif
		)
		solved = r;
	else
		solved = l + "/" + r;
	
	if (solved[solved.length() - 1] == separator ||
		solved[solved.length() - 1] == '/')
		solved.erase(solved.end() - 1);
	str_replace(std::string() + separator, "/", solved);
	str_replace("//", "/", solved);
	//~ str_replace("./", "", solved);
	std::cout << "START1\t: " << solved << std::endl;
	if (solved.substr(solved.length() - 2) == "/.")
		solved.erase(solved.end() - 2, solved.end());
	std::cout << "START2\t: " << solved << std::endl;
	std::cout << "TUTU\t" << solved.rfind("/", solved.find("/..")) << std::endl;
	while ((posParent = solved.find("/..")) != std::string::npos)
		std::cout << "TUTU\t: " << solved.erase(solved.rfind("/", posParent) + 1, posParent + 2) << std::endl;
	std::cout << "END\t: " << solved << std::endl;
#else
	char				actualpath[PATH_MAX + 1];
	
	if (r[0] == '/')
		solved = r;
	else
		solved = l + "/" + r;
	
	(void)::realpath(solved.c_str(), actualpath);
	
	solved = actualpath;
#endif
	
	return (solved);
}

std::string				path_parse( std::string const & path, std::string const & search ) // return info of file path
{
#ifdef	_WIN32
	std::string			drive_letter;
#endif
	std::string			path_directory;
	std::string			parent_directory;
	std::string			file;
	std::string			extension;
	size_t				t_n;
	char *				buffcwd;
	std::string			solved;
	struct stat			buffStat;
	unsigned int		i;
	
	if (stat(path.c_str(), &buffStat) != 0)
	{
		(void)path;(void)search;
		return ("");
	}
	else
	{
		buffcwd = ::getcwd(NULL, 0);
		solved = solve_path(buffcwd, path);
		::free(buffcwd);
	
#ifdef	_WIN32
		drive_letter = solved[0];
#endif
		t_n = solved.rfind("/");
		path_directory = solved.substr(0, t_n);
		parent_directory = path_directory.substr(path_directory.rfind("/", t_n) + 1);
		file = solved.substr(t_n + 1);
		extension = file.substr(file.rfind(".") + 1);
		for (i = 0; i < extension.length(); i++)
			extension[i] = ::tolower(extension[i]);
	}
	
	if (search == "path_directory")
	{
		return (path_directory);
	}
#ifdef	_WIN32
	else if (search == "drive_letter")
	{
		return (drive_letter);
	}
#endif
	else if (search == "parent_directory")
	{
		return (parent_directory);
	}
	else if (search == "file")
	{
		return (file);
	}
	else if (search == "extension")
	{
		return (extension);
	}
	else
	{
		return (solved);
	}
}
