#include	<cerrno>
#include	<cstring>
#include	<fstream>
#include	<sstream>
#include	<iostream>
#include	"time_cross.h"

#include	"usual/printLog.hh"

namespace	std
{
	void				init_printLog( char const * _fileName )
	{
		static std::string		fileName;
		static std::streambuf *	backup = std::clog.rdbuf();
		static std::ofstream	ofs;
		
		if (_fileName == NULL)
		{
			if (ofs.is_open())
			{
				bool			empty;
				
				ofs.seekp(0, std::ios_base::end);
				empty = (static_cast< long >(ofs.tellp()) == 0);
				std::clog.rdbuf(backup);
				ofs.close();
				if (empty)
					if (::remove(fileName.c_str()) == -1)
						throw (strerror(errno));
			}
		}
		else
		{
			if (ofs.is_open())
				ofs.close();
			ofs.open(_fileName, std::fstream::out | std::fstream::app);
			if (!(ofs.is_open()))
			{
				std::string		t_s;
				
				t_s += "init_printLog throw with fileName : ";
				t_s += _fileName;
				t_s += " ;";
				throw (t_s);
			}
			fileName = _fileName;
			std::clog.rdbuf(ofs.rdbuf());
			
# ifdef	__INFO
			std::clog << std::endl << std::ctime << "Initiate new Log ====" << std::endl;
# endif
		}
	};
	
	ostream &			log( ostream & os )
	{
		os << "LOG : ";
		
		return (os);
	};
	ostream &			warning( ostream & os )
	{
		os << "WARNING : ";
		
		return (os);
	};
	ostream &			error( ostream & os )
	{
		os << "ERROR : ";
		
		return (os);
	};
	ostream &			raise( ostream & os )
	{
		os << " << RAISED." << std::endl << std::flush;
		throw (-1);
		
		return (os);
	};
	ostream &			ctime( ostream & os )
	{
		//~ ::time_t					rawtime;
		//~ std::string					t_str;
		//~ 
		//~ ::time(&rawtime);
		//~ t_str = ::asctime(::localtime(&rawtime));
		//~ if (t_str.length() > 0 && t_str[t_str.length() - 1] == '\n')
			//~ t_str = t_str.substr(0, t_str.length() - 1);
		//~ os << t_str << " : ";
		
		return (os);
	};
};
