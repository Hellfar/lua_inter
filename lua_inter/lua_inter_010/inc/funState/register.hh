#pragma once

#ifndef		__REGISTER_HH__
# define	__REGISTER_HH__

# include	<lua/lua.hpp>

void					(registerToLua)			( lua_State * L );

#endif
