#pragma	once

#ifndef		__SIGNAL_CROSS_H__
# define	__SIGNAL_CROSS_H__

# include	<iostream>
# include	"usual/printLog.hh"

# if		defined(_WIN32)

#  include	<windows.h>

//# define	signal(sig, func)		(::SetUnhandledExceptionFilter((func)))

LONG WINAPI							sigCatch( EXCEPTION_POINTERS * pExcept )
{
	static char msg[256];

	(void)sprintf_s(msg, 256, "Unhandled exception 0x%08x at 0x%08x",
				pExcept->ExceptionRecord->ExceptionCode,
				pExcept->ExceptionRecord->ExceptionAddress
			);

	std::clog << std::error << "Signal catched(" << msg << ")." << std::raise;

	return (EXCEPTION_EXECUTE_HANDLER);
}

# elif		defined(__GNUC__) && defined(__unix__) && defined(__linux__)

#  include	<signal.h>

void								sigCatch( int signal )
{
	std::clog << std::error << "Signal catched(" << signal << ")." << std::raise;
}

# endif

void								init_sigCatch( void )
{

# if		defined(_WIN32)

	::SetUnhandledExceptionFilter(&sigCatch);

# elif		defined(__GNUC__) && defined(__unix__) && defined(__linux__)

	signal(SIGSEGV, &sigCatch);

# endif

}

#endif
