#pragma once

#ifndef		__LUA_INTER_HH__
# define	__LUA_INTER_HH__

# include	<utility>
# include	<queue>
# include	<vector>
# include	<map>
# include	<string>
# include	<lua/lua.hpp>
# include	"I_Console.hh"
# include	"conf.hh"

# include	"I_Lua_inter.hh"

class								Lua_inter:
	public							I_Lua_inter
{
private:
	lua_State *						state;
	
	std::string						argsControl;
	std::string						args;
	
	std::queue< std::pair< std::pair< enum I_Lua_inter::e_typeEventHistory, std::string >, std::queue< std::string > > >	history;
	
	bool							stateSTDLUALib;
	
	std::string						currentDir;
	std::string						currentFile;
	
	std::vector<TYPELIB>			aLibRef;
	
	I_Console *						console;
	
public:
	(Lua_inter)											( std::string const & _argsControl = "{}" );
	(Lua_inter)											( Lua_inter const & other );
	virtual (~Lua_inter)								();
	
public:
	virtual Lua_inter &				(operator=)			( Lua_inter const & other );
	
public:
	virtual bool					(dostr)				( std::string const & str = "" );
	virtual bool					(dofl)				( std::string const & file = DEFAULT_FILENAME );
	//~ virtual bool					(run)				( std::string const & file = DEFAULT_FILENAME ) __attribute__ ((weak, alias ("_ZN9Lua_inter4doflERKSs")));
	virtual void					(load_stdLibs)		( void );
	
public:
	virtual lua_State *				(getState)			( void ) const;
	virtual std::string const &		(getArgsControl)	( void ) const;
	virtual std::string const &		(getArgs)			( void ) const;
	virtual std::queue< std::pair< std::pair< enum I_Lua_inter::e_typeEventHistory, std::string >, std::queue< std::string > > > const &	(getHistory)	( void ) const;
	virtual int						(getReturn)			( void ) const;
	virtual std::string const &		(getCurrentDir)		( void ) const;
	virtual std::string const &		(getCurrentFile)	( void ) const;
	//~ virtual std::vector<TYPELIB> &	(getALibRef)		( void );
	virtual int						(addLibRef)			( std::string const & nameLib, std::string const & sparams = "" );
	virtual void					(eraseLibRef)		( unsigned int pos );
	virtual void					(clearALibRef)		( void );
	
	virtual I_Console *				(getConsole)		( void ) const;
	virtual void					(setConsole)		( bool state );
};

typedef bool						(*type_LIB_init)	( I_Lua_inter *, std::string const & );

#endif
