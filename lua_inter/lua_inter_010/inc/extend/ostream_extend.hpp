#ifndef		__OSTREAM_EXTEND_HPP__
# define	__OSTREAM_EXTEND_HPP__

# include	<ostream>

namespace	std
{
	ostream &				operator<<( ostream & o, char * * a_ptr )
	{
		while (*a_ptr != NULL)
		{
			o << "\"" << *(a_ptr++) << "\"";
			if (*a_ptr != NULL)
				o << ",";
		}
		
		return (o);
	}
};

#endif
