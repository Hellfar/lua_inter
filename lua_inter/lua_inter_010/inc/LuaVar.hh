#pragma once

#ifndef		__LUAVAR_HH__
# define	__LUAVAR_HH__

# define	lua_unregister(L, n)    \
		(lua_pushnil(L), lua_setglobal(L, n))
# define	lua_setConst(L, name)   \
		{ lua_pushnumber(L, name); lua_setglobal(L, #name); }

/*class				LuaVar
{
	private:
	std::string		name;
	std::string		typeVal;
	bool			activeVal;
	
	bool			boolVal;
	int				integerVal;
	float			floatVal;
	std::string		stringVal;
	
	
	public:
	LuaVar();
	LuaVar( bool val );
	LuaVar( int val );
	LuaVar( float val );
	LuaVar( std::string val );
	LuaVar( lua_State * state, std::string nameVar );
	
	void			putLuaVar( lua_State *state, std::string nameVar );
	void			putVal();
	void			putVal( bool val );
	void			putVal( int val );
	void			putVal( float val );
	void			putVal( std::string val );
	
	void refresh();
	
	void			getLuaVar( lua_State *state, std::string nameVar );
	std::string		getType();
	bool			isReadable();
	bool			getBool();
	int				getInt();
	float			getFloat();
	std::string		getString();
};*/

#endif
