#pragma	once

#ifndef		__CONSOLE_HH__
# define	__CONSOLE_HH__

# include	"unistd_cross.h"
# include	<string>

# include	"conf.hh"

# include	"I_Console.hh"

class							Console: public		I_Console
{
private:
	FDIN_FILENO					hstdin;
	FDOUT_FILENO				hstdout;
	FDERR_FILENO				hstderr;
	std::string					title;
	WINDOWS_FILENO				hwnd;
	
public:
	(Console)											();
	virtual (~Console)									();
	
public:
	virtual std::string const &	(getTitle)				( void ) const;
	virtual void				(setTitle)				( std::string const & title );
	virtual void				(setDefaultTextColor)	( int color = DEFAULT_COLOR_CONSOLE );
	
public:
	virtual void				(outflow)				( std::string const & str, int color = -1 ) const;
	virtual std::string			(incflow)				( int max = -1 ) const;
	
// public:
	// friend std::string const &		(operator<<)( Console const & inst, std::string const & str );
	// friend std::string const		(operator>>)( Console const & inst, std::string const & str );
};

// std::string const &		(operator<<)( Console const & inst, std::string const & str );
// std::string const		(operator>>)( Console const & inst, std::string const & str );

#endif
