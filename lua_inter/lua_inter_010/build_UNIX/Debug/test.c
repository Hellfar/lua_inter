#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int					main(int argc, char **argv)
{
	if (argc > 1)
	{
		void *			handle;
		char *			lib = argv[1];
		
		if (NULL == (handle = dlopen(lib, RTLD_LAZY)))
		{
			(void)fprintf(stderr, "%s\n", dlerror());
			exit(EXIT_FAILURE);
		}
		
		dlerror();
		
		dlclose(handle);
	}
	else
		(void)fprintf(stderr, "USAGE: %s LIBFULLPATH\n", argv[0]);
	
	
	/* Writing: cosine = (double (*)(double)) dlsym(handle, "cos");
		would seem more natural, but the C99 standard leaves
		casting from "void *" to a function pointer undefined.
		The assignment used below is the POSIX.1-2003 (Technical
		Corrigendum 1) workaround; see the Rationale for the
		POSIX specification of dlsym(). */
	
	//~ *(void **) (&cosine) = dlsym(handle, "cos");
	//~ 
	//~ if ((error = dlerror()) != NULL)  {
		//~ fprintf(stderr, "%s\n", error);
		//~ exit(EXIT_FAILURE);
	//~ }
	//~ 
	//~ printf("%f\n", (*cosine)(2.0));
	return (EXIT_SUCCESS);
}
