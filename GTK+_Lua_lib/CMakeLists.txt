CMAKE_MINIMUM_REQUIRED (VERSION 2.6)

PROJECT ("GTKP_Lua")

# SET (CMAKE_BUILD_TYPE	"Release")
SET (CMAKE_BUILD_TYPE	"Debug")

if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
	ADD_DEFINITIONS ("-D__DEBUG")
endif ()

ADD_DEFINITIONS ("-DBUILD_DLL")

FILE (MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}")
SET (LIBRARY_OUTPUT_PATH "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}")

# directories
SET (PROJECT_HDR_DIR	"${PROJECT_SOURCE_DIR}/inc")
SET (PROJECT_SRC_DIR	"${PROJECT_SOURCE_DIR}/src")
SET (PROJECT_LIB_DIR	"${PROJECT_SOURCE_DIR}/lib")

# sources
INCLUDE_DIRECTORIES ("${PROJECT_HDR_DIR}")
FILE (GLOB	HDR			"${PROJECT_HDR_DIR}/unistd.h"
						"${PROJECT_HDR_DIR}/time.h"
						"${PROJECT_HDR_DIR}/usual/std_fun.hh"
						"${PROJECT_HDR_DIR}/usual/printLog.hh"
						"${PROJECT_HDR_DIR}/conf.hh"
						"${PROJECT_HDR_DIR}/main.hh"
						"${PROJECT_HDR_DIR}/I_Console.hh"
						"${PROJECT_HDR_DIR}/I_Lua_inter.hh"
						# "${PROJECT_HDR_DIR}/container/Something.hh"
						"${PROJECT_HDR_DIR}/funState/register.hh"
						"${PROJECT_HDR_DIR}/funState/under_funStateLua.hh"
						"${PROJECT_HDR_DIR}/funState/funStateLua.hh"
						"${PROJECT_HDR_DIR}/funState/funStateLua_lib.hh"
	)
FILE (GLOB	SRC			"${PROJECT_SRC_DIR}/main.cpp"
						# "${PROJECT_SRC_DIR}/container/Something.cpp"
						"${PROJECT_SRC_DIR}/funState/register.cpp"
						"${PROJECT_SRC_DIR}/funState/funStateLua_lib.cpp"
	)

# libraries
INCLUDE_DIRECTORIES ("${PROJECT_LIB_DIR}/inc")
LINK_DIRECTORIES ("${PROJECT_LIB_DIR}/bin")

if ("${CMAKE_SYSTEM_NAME}" MATCHES "Linux")
	SET (CC		"gcc")
	SET (CXX	"g++")

	SET (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -W -Wall -Wextra")
	SET (CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}")

	#~ find_package (gtk+-2.0 REQUIRED)
	EXEC_PROGRAM (pkg-config ARGS --cflags --libs gtk+-2.0 OUTPUT_VARIABLE GTKMM_PKG_FLAGS)
	SET (GTKMM_PKG_FLAGS "${GTKMM_PKG_FLAGS}" CACHE STRING "GTKMM Flags")
	SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GTKMM_PKG_FLAGS}")
	SET (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
endif ()

# bin
ADD_LIBRARY (${PROJECT_NAME}
			SHARED
			${HDR}
			${SRC}
			)

# links
if ("${CMAKE_SYSTEM_NAME}" MATCHES "Linux")
	TARGET_LINK_LIBRARIES (${PROJECT_NAME} "dl")
endif ()
TARGET_LINK_LIBRARIES (${PROJECT_NAME} "lua52")
TARGET_LINK_LIBRARIES (${PROJECT_NAME} ${GTKMM_PKG_FLAGS})
