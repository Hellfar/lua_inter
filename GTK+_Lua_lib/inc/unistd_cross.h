#pragma	once

#ifndef		__UNISTD_CROSS_H__
# define	__UNISTD_CROSS_H__

# include	<unistd.h>

# ifdef	_WIN32

#  include	<direct.h>

#  define	chdir		_chdir
#  define	getcwd		_getcwd

typedef		HANDLE	FDOUT_FILENO;
typedef		HANDLE	FDIN_FILENO;
typedef		HANDLE	FDERR_FILENO;
typedef		HWND	WINDOWS_FILENO;

# elif		defined(__GNUC__) && defined(__unix__) && defined(__linux__)

typedef		int		FDOUT_FILENO;
typedef		int		FDIN_FILENO;
typedef		int		FDERR_FILENO;
typedef		int		WINDOWS_FILENO;

# endif

#endif
