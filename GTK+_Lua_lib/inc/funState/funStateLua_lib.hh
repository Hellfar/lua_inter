#pragma	once

#ifndef		__FUNSTATELUA_HH__
# define	__FUNSTATELUA_HH__

#include	<lua/lua.hpp>

int						(gtk_window_new_lua)	( lua_State * L );
int						(gtk_hbox_new_lua)	  ( lua_State * L );
int						(gtk_vbox_new_lua)	  ( lua_State * L );
int						(gtk_box_new_lua)		( lua_State * L );
int           (g_signal_connect_lua)    ( lua_State * L );
int						(gtk_widget_show_lua)	( lua_State * L );
int           (gtk_widget_show_all_lua) ( lua_State * L );
int						(gtk_main_lua)			( lua_State * L );

#endif
