#pragma	once

#ifndef		__TIME_CROSS_H__
# define	__TIME_CROSS_H__

# if		defined(_WIN32)
#  define	asctime			asctime_s
# elif		defined(__GNUC__) && defined(__unix__) && defined(__linux__)
#  include	<ctime>
# else
#  error	System not suported.
# endif

#endif
