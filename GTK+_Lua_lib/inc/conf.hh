#pragma	once

#ifndef		__MAIN_HPP__
# define	__MAIN_HPP__

# include	<lua/lua.hpp>

/*
# define	__INFO
//*/

# define	DEFAULT_FILENAME		"index.lua"
# define	DEFAULT_PRGM_VARNAME	"oprgm"
# define	DEFAULT_RETURN_VARNAME	"ProcessMod"
# define	DEFAULT_MAIN_FUNCTION	"main"

# define	DEFAULT_LIB_INIT		"processAtLoad"
# define	DEFAULT_LIB_UNINIT		"processAtUnload"

//MESSAGE LOG
# define	UNKNOWN_MSGLOG			"Unknown error."
# define	INV_NB_ARG_MSGLOG		"Number of arguments are invalid."
# define	INV_TYPE_ARG_MSGLOG		"Type of arguments are invalid."
# define	CONSOLE_ABSENT			"Missing console."

//CONSOLE
# define	DEFAULT_COLOR_CONSOLE	0x07

#endif
