#include	<string>
#include	<lua/lua.hpp>
#include	<gtk/gtk.h>
#include	"funState/funStateLua_lib.hh"
#include	"funState/register.hh"
#include	"I_Lua_inter.hh"

#include	"main.hh"

bool							GTKP_Lua = false;

bool DLL_EXPORT					processAtLoad( I_Lua_inter * oprgm, std::string const & paramForLoad )
{
	int							argc = *(ext_s_args.argc);
	char * *					argv = *(ext_s_args.argv);

	(void)paramForLoad;

	if (GTKP_Lua == false)
	{
		if (gtk_init_check(&argc, &argv) == TRUE)
		{
			registerToLua_lib(oprgm->getState());
			GTKP_Lua = true;
		}
	}

	return (GTKP_Lua);
}

bool DLL_EXPORT					processAtUnload( I_Lua_inter * oprgm, std::string const & paramForLoad )
{
	(void)oprgm;
	(void)paramForLoad;

	if (GTKP_Lua == true)
    {
		GTKP_Lua = false;
		unregisterToLua_lib(oprgm->getState());
    }

	return (true);
}
