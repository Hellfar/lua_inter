#include	<lua/lua.hpp>
#include	<gtk/gtk.h>
#include	"main.hh"
#include	"funState/funStateLua_lib.hh"

#include	"funState/register.hh"

void				registerToLua_lib( lua_State * L )
{
	// func ----------------------------------------------------------------
	lua_register(L, "gtk_window_new", &gtk_window_new_lua);
	lua_register(L, "gtk_hbox_new", &gtk_hbox_new_lua);
	lua_register(L, "gtk_vbox_new", &gtk_vbox_new_lua);
	// lua_register(L, "gtk_box_new", &gtk_box_new_lua);
	lua_register(L, "g_signal_connect", &g_signal_connect_lua);
	lua_register(L, "gtk_widget_show", &gtk_widget_show_lua);
	lua_register(L, "gtk_widget_show_all", &gtk_widget_show_lua);
	lua_register(L, "gtk_main", &gtk_main_lua);

	// const ----------------------------------------------------------------
	lua_setConst(L, TRUE);
	lua_setConst(L, FALSE);
	lua_setConst(L, GTK_WINDOW_TOPLEVEL);
	lua_setConst(L, GTK_ORIENTATION_HORIZONTAL);
	lua_setConst(L, GTK_ORIENTATION_VERTICAL);
}

void				unregisterToLua_lib( lua_State * L )
{
	// func ----------------------------------------------------------------
	lua_unregister(L, "gtk_window_new");
	lua_unregister(L, "gtk_box_new");
	lua_unregister(L, "gtk_widget_show");
	lua_unregister(L, "gtk_widget_show_all");
	lua_unregister(L, "gtk_main");

	// const ----------------------------------------------------------------
	lua_unregister(L, "TRUE");
	lua_unregister(L, "FALSE");
	lua_unregister(L, "GTK_WINDOW_TOPLEVEL");
	lua_unregister(L, "GTK_ORIENTATION_HORIZONTAL");
	lua_unregister(L, "GTK_ORIENTATION_VERTICAL");
}
