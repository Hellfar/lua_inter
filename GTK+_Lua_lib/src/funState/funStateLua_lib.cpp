#include	<sstream>
#include	<lua/lua.hpp>
#include	<gtk/gtk.h>
#include	"usual/printLog.hh"
#include	"conf.hh"

#include	"funState/funStateLua_lib.hh"

int						gtk_window_new_lua( lua_State * L )
{
	std::stringstream	ss_widget;
	lua_Number			t_l_widget;
	GtkWindowType		type;
	int					nbArguments = lua_gettop(L);

	if (nbArguments == 1)
	{
		if (lua_isnumber(L, 1))
			type = static_cast< GtkWindowType >(lua_tonumber(L, 1));
		else
		{
			std::clog << std::error << "gtk_window_new : " << INV_TYPE_ARG_MSGLOG << std::endl;

			lua_pushnil(L);

			return (1);
		}
	}
	else
		type = GTK_WINDOW_TOPLEVEL;

	ss_widget << reinterpret_cast< size_t >(gtk_window_new(type));
	ss_widget >> t_l_widget;
	lua_pushnumber(L, t_l_widget);

	return (1);
}

int						gtk_hbox_new_lua( lua_State * L )
{

	std::stringstream	ss_widget;
	lua_Number			t_l_widget;
	gboolean			homogeneous = true;
	gint				spacing = 0;
	int					nbArguments = lua_gettop(L);

	if (nbArguments > 2)
	{
		std::clog << std::error << "gtk_hbox_new : " << INV_NB_ARG_MSGLOG << std::endl;

		lua_pushnil(L);

		return (1);
	}
	else if (nbArguments > 0 && lua_isboolean(L, 1))
	{
		homogeneous = static_cast< gboolean >(lua_toboolean(L, 1));
		if (nbArguments > 1 && lua_isnumber(L, 2))
			spacing = static_cast< GtkOrientation >(lua_tonumber(L, 2));
	}

	ss_widget << reinterpret_cast< size_t >(gtk_hbox_new(homogeneous, spacing));
	ss_widget >> t_l_widget;
	lua_pushnumber(L, t_l_widget);

	return (1);
}

int						gtk_vbox_new_lua( lua_State * L )
{

	std::stringstream	ss_widget;
	lua_Number			t_l_widget;
	gboolean			homogeneous = true;
	gint				spacing = 0;
	int					nbArguments = lua_gettop(L);

	if (nbArguments > 2)
	{
		std::clog << std::error << "gtk_vbox_new : " << INV_NB_ARG_MSGLOG << std::endl;

		lua_pushnil(L);

		return (1);
	}
	else if (nbArguments > 0 && lua_isboolean(L, 1))
	{
		// orientation = static_cast< GtkOrientation >(lua_toboolean(L, 1));
		if (nbArguments > 1 && lua_isnumber(L, 2))
			spacing = static_cast< gint >(lua_tonumber(L, 2));
	}

	ss_widget << reinterpret_cast< size_t >(gtk_vbox_new(homogeneous, spacing));
	ss_widget >> t_l_widget;
	lua_pushnumber(L, t_l_widget);

	return (1);
}

/* GTK Version 3.0
int						gtk_box_new_lua( lua_State * L )
{
	std::stringstream	ss_widget;
	lua_Number			t_l_widget;
	GtkOrientation		orientation = GTK_ORIENTATION_HORIZONTAL;
	gint				spacing = 0;
	int					nbArguments = lua_gettop(L);

	if (nbArguments > 2)
	{
		std::clog << std::error << "gtk_box_new : " << INV_NB_ARG_MSGLOG << std::endl;

		lua_pushnil(L);

		return (1);
	}
	else if (nbArguments > 0 && lua_isnumber(L, 1))
	{
		orientation = static_cast< GtkOrientation >(lua_tonumber(L, 1));
		if (nbArguments > 1 && lua_isnumber(L, 2))
			spacing = static_cast< gint >(lua_tonumber(L, 2));
	}

	ss_widget << reinterpret_cast< size_t >(gtk_box_new(orientation, spacing));
	ss_widget >> t_l_widget;
	lua_pushnumber(L, t_l_widget);

	return (1);
}
*/

int           g_signal_connect_lua( lua_State * L )
{
	GtkWidget *			widget;
	int					nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isnumber(L, 1))
		widget = reinterpret_cast< GtkWidget * >(static_cast< unsigned int >(lua_tonumber(L, 1)));
	else
	{
		std::clog << std::error << "g_signal_connect : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;

		lua_pushnil(L);

		return (1);
	}

  g_signal_connect (widget, "destroy", G_CALLBACK (gtk_main_quit), NULL);

  return (0);
}

int						gtk_widget_show_lua( lua_State * L )
{
	GtkWidget *			widget;
	int					nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isnumber(L, 1))
		widget = reinterpret_cast< GtkWidget * >(static_cast< unsigned int >(lua_tonumber(L, 1)));
	else
	{
		std::clog << std::error << "gtk_widget_show : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;

		lua_pushnil(L);

		return (1);
	}

	gtk_widget_show(widget);

	return (0);
}

int						gtk_widget_show_all_lua( lua_State * L )
{
	GtkWidget *			widget;
	int					nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isnumber(L, 1))
		widget = reinterpret_cast< GtkWidget * >(static_cast< unsigned int >(lua_tonumber(L, 1)));
	else
	{
		std::clog << std::error << "gtk_widget_show_all : " << INV_NB_ARG_MSGLOG << " + " << INV_TYPE_ARG_MSGLOG << std::endl;

		lua_pushnil(L);

		return (1);
	}

	gtk_widget_show_all(widget);

	return (0);
}

int						gtk_main_lua( lua_State * L )
{
	int					nbArguments = lua_gettop(L);

	if (nbArguments != 0)
		std::clog << std::warning << "gtk_main : " << INV_NB_ARG_MSGLOG << std::endl;

	gtk_main();

	return (0);
}
