#include	<cstdlib>
#include	<lua/lua.hpp>
#include	<SDL/SDL.h>
#include	<SDL/SDL_image.h>
#include	"usual/printLog.hh"
#include	"conf.hh"

#include	"funState/funStateLua_lib.hh"

struct
{
	SDL_Surface *	board;
	int				width;
	int				height;
	int				bitsperpixel;
	Uint32			flags;
}	default_sfc =
{
	NULL,
	640,
	480,
	32,
	SDL_HWSURFACE,
};

int					SDL_GetError_lua( lua_State * L )
{
	lua_pushstring(L, SDL_GetError());

	return (1);
}

int					SDL_SetVideoMode_lua( lua_State * L )
{
	int				width = default_sfc.width;
	int				height = default_sfc.height;
	int				bitsperpixel = default_sfc.bitsperpixel;
	Uint32			flags = default_sfc.flags;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		width = height = (int)lua_tonumber(L, 1);
	}

	if (nbArguments >= 2 && lua_isnumber(L, 2))
	{
		height = (int)lua_tonumber(L, 2);
	}

	if (nbArguments >= 3 && lua_isnumber(L, 3))
	{
		bitsperpixel = (unsigned int)lua_tonumber(L, 3);
	}

	if (nbArguments >= 4 && lua_isnumber(L, 4))
	{
		flags = (Uint32)lua_tonumber(L, 4);
	}

	default_sfc.board = SDL_SetVideoMode(width, height, bitsperpixel, flags);
	default_sfc.width = width;
	default_sfc.height = height;
	default_sfc.bitsperpixel = bitsperpixel;
	lua_pushnumber(L, (long unsigned int)(default_sfc.board));

	return (1);
}

int					SDL_FillRect_lua( lua_State * L )
{
	SDL_Surface *	dst = default_sfc.board;
	SDL_Rect *		dstrect = NULL;
	Uint32			color = 0;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		dst = (SDL_Surface *)((long unsigned int)lua_tonumber(L, 1));
	}

	if (nbArguments >= 2 && lua_isnumber(L, 2))
	{
		dstrect = (SDL_Rect *)((long unsigned int)lua_tonumber(L, 2));
	}

	if (nbArguments >= 3 && lua_isnumber(L, 3))
	{
		color = (Uint32)lua_tonumber(L, 3);
	}

	lua_pushnumber(L, SDL_FillRect(dst, dstrect, color));

	return (1);
}

int					SDL_CreateRGBSurface_lua( lua_State * L )
{
	Uint32			flags = SDL_HWSURFACE;
	int				width = default_sfc.width;
	int				height = default_sfc.height;
	int				bitsPerPixel = default_sfc.bitsperpixel;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		flags = (Uint32)lua_tonumber(L, 1);
	}

	if (nbArguments >= 2 && lua_isnumber(L, 2))
	{
		width = height = (int)lua_tonumber(L, 2);
	}

	if (nbArguments >= 3 && lua_isnumber(L, 3))
	{
		height = (int)lua_tonumber(L, 3);
	}

	if (nbArguments >= 4 && lua_isnumber(L, 4))
	{
		bitsPerPixel = (int)lua_tonumber(L, 4);
	}

	lua_pushnumber(L, (long unsigned int)SDL_CreateRGBSurface(flags, width, height, bitsPerPixel, 0, 0, 0, 0));

	return (1);
}

int					SDL_LoadBMP_lua( lua_State * L )
{
	char const *	file = NULL;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isstring(L, 1))
	{
		file = lua_tostring(L, 1);
	}

	lua_pushnumber(L, (long unsigned int)SDL_LoadBMP(file));

	return (1);
}

int					IMG_Load_lua( lua_State * L )
{
	char const *	file = NULL;
	SDL_Surface *	surface;

	int				nbArguments = lua_gettop(L);

	if (nbArguments == 1 && lua_isstring(L, 1))
	{
		file = lua_tostring(L, 1);
		surface = IMG_Load(file);
		if (surface == NULL)
		{
			lua_pushstring(L, IMG_GetError());
			lua_setglobal(L, "__ERROR__");

			lua_pushnil(L);
			return (1);
		}
	}
	else
	{
		lua_pushnil(L);
		return (1);
	}

	lua_pushnumber(L, (long unsigned int)surface);

	return (1);
}

int					IMG_GetError_lua( lua_State * L )
{
	int				nbArguments = lua_gettop(L);

	if (nbArguments != 0)
		lua_pushnil(L);
	else
		lua_pushstring(L, IMG_GetError());

	return (1);
}

int					SDL_FreeSurface_lua( lua_State * L )
{
	SDL_Surface *	surface = NULL;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		surface = (SDL_Surface *)((long unsigned int)lua_tonumber(L, 1));
	}

	SDL_FreeSurface(surface);

	return (0);
}

int					SDL_BlitSurface_lua( lua_State * L )
{
	SDL_Surface *	src = NULL;
	SDL_Rect		srcrect = {0, 0, 0, 0};
	SDL_Surface *	dst = NULL;
	SDL_Rect		dstrect = {0, 0, 0, 0};

	size_t			tableLength = 0;
	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		src = (SDL_Surface *)((long unsigned int)lua_tonumber(L, 1));
	}

	if (nbArguments >= 2 && lua_isnumber(L, 2))
	{
		srcrect.x = static_cast< Sint16 >(lua_tonumber(L, 2));
		srcrect.y = static_cast< Sint16 >(lua_tonumber(L, 2));
	}
	else if (nbArguments >= 2 && lua_istable(L, 2))
	{
		tableLength = lua_rawlen(L, 2);

		if (tableLength >= 1)
		{
			lua_pushnumber(L, 1);
			lua_gettable(L, 2);
			if (lua_isnumber(L, -1))
				srcrect.x = srcrect.y = static_cast< Sint16 >(lua_tonumber(L, -1));
			lua_pop(L, 1);
		}

		if (tableLength >= 2)
		{
			lua_pushnumber(L, 2);
			lua_gettable(L, 2);
			if (lua_isnumber(L, -1))
				srcrect.y = static_cast< Sint16 >(lua_tonumber(L, -1));
			lua_pop(L, 1);
		}

		if (tableLength >= 3)
		{
			lua_pushnumber(L, 3);
			lua_gettable(L, 2);
			if (lua_isnumber(L, -1))
				srcrect.w = srcrect.h = static_cast< Sint16 >(lua_tonumber(L, -1));
			lua_pop(L, 1);
		}

		if (tableLength >= 4)
		{
			lua_pushnumber(L, 4);
			lua_gettable(L, 2);
			if (lua_isnumber(L, -1))
				srcrect.h = static_cast< Sint16 >(lua_tonumber(L, -1));
			lua_pop(L, 1);
		}
	}

	if (nbArguments >= 3 && lua_isnumber(L, 3))
	{
		dst = (SDL_Surface *)(static_cast< long unsigned int >(lua_tonumber(L, 3)));
	}

	if (nbArguments >= 4 && lua_isnumber(L, 4))
	{
		dstrect.x = static_cast< Sint16 >(lua_tonumber(L, 4));
		dstrect.y = static_cast< Sint16 >(lua_tonumber(L, 4));
	}
	else if (nbArguments >= 4 && lua_istable(L, 4))
	{
		tableLength = lua_rawlen(L, 4);

		if (tableLength >= 1)
		{
			lua_pushnumber(L, 1);
			lua_gettable(L, 4);
			if (lua_isnumber(L, -1))
				dstrect.x = dstrect.y = static_cast< Sint16 >(lua_tonumber(L, -1));
			lua_pop(L, 1);
		}

		if (tableLength >= 2)
		{
			lua_pushnumber(L, 2);
			lua_gettable(L, 4);
			if (lua_isnumber(L, -1))
				dstrect.y = static_cast< Sint16 >(lua_tonumber(L, -1));
			lua_pop(L, 1);
		}

		if (tableLength >= 3)
		{
			lua_pushnumber(L, 3);
			lua_gettable(L, 4);
			if (lua_isnumber(L, -1))
				dstrect.w = dstrect.h = static_cast< Sint16 >(lua_tonumber(L, -1));
			lua_pop(L, 1);
		}

		if (tableLength >= 4)
		{
			lua_pushnumber(L, 4);
			lua_gettable(L, 4);
			if (lua_isnumber(L, -1))
				dstrect.h = static_cast< Sint16 >(lua_tonumber(L, -1));
			lua_pop(L, 1);
		}
	}

	lua_pushnumber(L, static_cast< long >(SDL_BlitSurface(src, &srcrect, dst, &dstrect)));

	return (1);
}

int					SDL_MapRGB_lua( lua_State * L )
{
	SDL_Surface *	board = default_sfc.board;
	Uint8			r = 0;
	Uint8			g = 0;
	Uint8			b = 0;
	Uint32			tmp_RGB = 0;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		board = ((SDL_Surface *)((long unsigned int)lua_tonumber(L, 1)));
	}

	if (nbArguments >= 2 && lua_isnumber(L, 2))
	{
		tmp_RGB = (Uint32)lua_tonumber(L, 2);
		r = (tmp_RGB & 0xFF0000) / 256 / 256;
		g = (tmp_RGB & 0x00FF00) / 256;
		b = tmp_RGB & 0x0000FF;
	}

	if (nbArguments >= 3 && lua_isnumber(L, 3))
	{
		r = (Uint8)lua_tonumber(L, 2);
		g = (Uint8)lua_tonumber(L, 3);
	}

	if (nbArguments >= 4 && lua_isnumber(L, 4))
	{
		b = (Uint8)lua_tonumber(L, 4);
	}

	if (board != NULL)
	{
		lua_pushnumber(L, SDL_MapRGB(board->format, r, g, b));

		return (1);
	}

	lua_pushnil(L);

	return (1);
}

int					SDL_SetColorKey_lua( lua_State * L )
{
	SDL_Surface *	surface = NULL;
	Uint32			flag = 0;
	Uint32			key = 0;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		surface = (SDL_Surface *)((long unsigned int)lua_tonumber(L, 1));
	}
  else
  {
    std::clog << std::error << "SDL_SetColorKey : " << INV_TYPE_ARG_MSGLOG << std::endl;

		lua_pushnil(L);

		return (1);
  }

	if (nbArguments >= 2 && lua_isnumber(L, 2))
	{
		flag = (Uint32)lua_tonumber(L, 2);
	}

	if (nbArguments >= 3 && lua_isnumber(L, 3))
	{
		key = (Uint32)lua_tonumber(L, 3);
	}

	lua_pushnumber(L, SDL_SetColorKey(surface, flag, key));

	return (1);
}

int					SDL_SetAlpha_lua( lua_State * L )
{
	SDL_Surface *	surface = NULL;
	Uint32			flags = SDL_SRCALPHA;
	Uint8			alpha = 128;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		surface = (SDL_Surface *)((long unsigned int)lua_tonumber(L, 1));
	}

	if (nbArguments >= 2 && lua_isnumber(L, 2))
	{
		flags = (Uint32)lua_tonumber(L, 2);
	}

	if (nbArguments >= 3 && lua_isnumber(L, 3))
	{
		alpha = (Uint8)lua_tonumber(L, 3);
	}

	lua_pushnumber(L, SDL_SetAlpha(surface, flags, alpha));

	return (1);
}

int					SDL_Flip_lua( lua_State * L )
{
	SDL_Surface *	screen = default_sfc.board;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isnumber(L, 1))
	{
		screen = (SDL_Surface *)((long unsigned int)lua_tonumber(L, 1));
	}

	lua_pushnumber(L, SDL_Flip(screen));

	return (1);
}

int					SDL_WM_SetCaption_lua( lua_State * L )
{
	char const *	title = "DLL_app";
	char const *	icon = NULL;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isstring(L, 1))
	{
		title = lua_tostring(L, 1);
	}

	if (nbArguments >= 2 && lua_isstring(L, 2))
	{
		icon = lua_tostring(L, 2);
	}

	SDL_WM_SetCaption(title, icon);

	return (0);
}

int					SDL_WM_SetIcon_lua( lua_State * L )
{
	SDL_Surface *	icon = NULL;
	Uint8 *			mask = NULL;

	int				nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isstring(L, 1))
	{
		icon = (SDL_Surface *)((long unsigned int)lua_tonumber(L, 1));
	}

	if (nbArguments >= 2 && lua_isstring(L, 2))
	{
		mask = (Uint8 *)((long unsigned int)lua_tonumber(L, 2));
	}

	SDL_WM_SetIcon(icon, mask);

	return (0);
}

int					SDL_WaitEvent_lua( lua_State * L )
{
	SDL_Event		event;

	SDL_WaitEvent(&event);

	lua_newtable(L);

	// type
	lua_pushstring(L, "type");
	lua_pushnumber(L, event.type);
	lua_settable(L, -3);

//active

	// key
	lua_pushstring(L, "key");
	lua_newtable(L);
		lua_pushstring(L, "type");
		lua_pushnumber(L, event.key.type);
		lua_settable(L, -3);
		lua_pushstring(L, "state");
		lua_pushnumber(L, event.key.state);
		lua_settable(L, -3);
		lua_pushstring(L, "keysym");
		lua_newtable(L);
			lua_pushstring(L, "scancode");
			lua_pushnumber(L, event.key.keysym.scancode);
			lua_settable(L, -3);
			lua_pushstring(L, "sym");
			lua_pushnumber(L, event.key.keysym.sym);
			lua_settable(L, -3);
			lua_pushstring(L, "mod");
			lua_pushnumber(L, event.key.keysym.mod);
			lua_settable(L, -3);
			lua_pushstring(L, "unicode");
			lua_pushnumber(L, event.key.keysym.unicode);
			lua_settable(L, -3);
		lua_settable(L, -3);
	lua_settable(L, -3);

//motion
//button
//jaxis
//jball
//jhat
//jbutton

	// resize
	lua_pushstring(L, "resize");
	lua_newtable(L);
		lua_pushstring(L, "w");
		lua_pushnumber(L, event.resize.w);
		lua_settable(L, -3);
		lua_pushstring(L, "h");
		lua_pushnumber(L, event.resize.h);
		lua_settable(L, -3);
	lua_settable(L, -3);

//expose
//quit
//user
//syswm

    return (1);
}

int					SDL_PollEvent_lua( lua_State * L )
{
	SDL_Event		event;

	SDL_PollEvent(&event);

	lua_newtable(L);

	// type
	lua_pushstring(L, "type");
	lua_pushnumber(L, event.type);
	lua_settable(L, -3);

//active

	// key
	lua_pushstring(L, "key");
	lua_newtable(L);
		lua_pushstring(L, "type");
		lua_pushnumber(L, event.key.type);
		lua_settable(L, -3);
		lua_pushstring(L, "state");
		lua_pushnumber(L, event.key.state);
		lua_settable(L, -3);
		lua_pushstring(L, "keysym");
		lua_newtable(L);
			lua_pushstring(L, "scancode");
			lua_pushnumber(L, event.key.keysym.scancode);
			lua_settable(L, -3);
			lua_pushstring(L, "sym");
			lua_pushnumber(L, event.key.keysym.sym);
			lua_settable(L, -3);
			lua_pushstring(L, "mod");
			lua_pushnumber(L, event.key.keysym.mod);
			lua_settable(L, -3);
			lua_pushstring(L, "unicode");
			lua_pushnumber(L, event.key.keysym.unicode);
			lua_settable(L, -3);
		lua_settable(L, -3);
	lua_settable(L, -3);

//motion
//button
//jaxis
//jball
//jhat
//jbutton

	// resize
	lua_pushstring(L, "resize");
	lua_newtable(L);
		lua_pushstring(L, "w");
		lua_pushnumber(L, event.resize.w);
		lua_settable(L, -3);
		lua_pushstring(L, "h");
		lua_pushnumber(L, event.resize.h);
		lua_settable(L, -3);
	lua_settable(L, -3);

//expose
//quit
//user
//syswm

    return (1);
}

int					SDL_Delay_lua( lua_State * L )
{
	Uint32			ms = 1000;

	int nbArguments = lua_gettop(L);

	if (nbArguments >= 1 && lua_isstring(L, 1))
	{
		ms = (Uint32)lua_tonumber(L, 1);
	}
	else if (nbArguments >= 1 && lua_isnil(L, 1))
	{
		ms = 0;
	}

	SDL_Delay(ms);

	return (0);
}
