#include	<string>
#include	<SDL/SDL.h>
#include	<lua/lua.hpp>
#include	"usual/printLog.hh"
#include	"funState/funStateLua_lib.hh"
#include	"funState/register.hh"
#include	"I_Lua_inter.hh"

#include	"main.hh"

bool							SDL_Lua_lib = false;
#include <iostream>
bool DLL_EXPORT					processAtLoad( I_Lua_inter * oprgm, std::string const & paramForLoad )
{std::cout << "SDL0" << std::endl << "oprgm in lib: " << (void *)oprgm << std::endl;
	if (!SDL_Lua_lib)
	{
		Uint32 initSDLflags = 0;
		if (paramForLoad.length())
		{std::cout << "SDL1" << std::endl;
			if (paramForLoad.find("v") != std::string::npos)
				initSDLflags = initSDLflags | SDL_INIT_VIDEO;
			if (paramForLoad.find("a") != std::string::npos)
				initSDLflags = initSDLflags | SDL_INIT_AUDIO;
			if (paramForLoad.find("c") != std::string::npos)
				initSDLflags = initSDLflags | SDL_INIT_CDROM;
			if (paramForLoad.find("j") != std::string::npos)
				initSDLflags = initSDLflags | SDL_INIT_JOYSTICK;
			if (paramForLoad.find("t") != std::string::npos)
				initSDLflags = initSDLflags | SDL_INIT_TIMER;
			if (paramForLoad.find("e") != std::string::npos)
				initSDLflags = initSDLflags | SDL_INIT_EVERYTHING;
std::cout << "SDL2" << std::endl;		}
		else
		{
			initSDLflags = SDL_INIT_VIDEO;
		}
std::cout << "SDL3" << std::endl;
		if (SDL_Init(initSDLflags) != -1)
		{
			SDL_Lua_lib = true;
std::cout << "SDL4" << std::endl << "L: " << (lua_State *)(oprgm->getState()) << std::endl;
			registerToLua_lib(oprgm->getState());std::cout << "SDL5" << std::endl;
		}
	}

	return (SDL_Lua_lib);
}

bool DLL_EXPORT					processAtUnload( I_Lua_inter * oprgm, std::string const & paramForLoad )
{
	(void)paramForLoad;
	if (SDL_Lua_lib)
	{
		unregisterToLua_lib(oprgm->getState());

		SDL_Quit();
	}

	return (true);
}
