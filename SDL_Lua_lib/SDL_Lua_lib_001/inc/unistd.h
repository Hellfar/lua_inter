#pragma	once

#ifndef		__UNISTD_CROSS_H__
# define	__UNISTD_CROSS_H__

# ifdef	_WIN32
#  include	<direct.h>
#  define	chdir		_chdir
#  define	getcwd		_getcwd
# else
#  include	<unistd.h>
# endif

#endif