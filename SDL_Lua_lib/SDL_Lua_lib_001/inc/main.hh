#pragma once

#ifndef		__MAIN_H__
# define	__MAIN_H__

# include	<sstream>
# include	<lua/lua.hpp>

# define	lua_unregister(L, n)	(lua_pushnil(L), lua_setglobal(L, n))
# define	lua_setConst(L, name)	{lua_pushnumber(L, name); lua_setglobal(L, #name);}

# include	"I_Lua_inter.hh"

# ifdef		BUILD_DLL
#  if		defined(_WIN32)
#   define	DLL_EXPORT				__declspec(dllexport)
#  else
#   define	DLL_EXPORT
#  endif
# else
#  if		defined(_WIN32)
#   define	DLL_EXPORT				__declspec(dllimport)
#  else
#   define	DLL_EXPORT
#  endif
# endif

# ifdef		__cplusplus
extern		"C"
{
# endif

bool DLL_EXPORT						(processAtLoad)		( I_Lua_inter * oprgm, std::string const & paramForLoad );
bool DLL_EXPORT						(processAtUnload)	( I_Lua_inter * oprgm, std::string const & paramForLoad );

# ifdef		__cplusplus
}
# endif

extern struct s_argsContainer {int * argc; char * * * argv;}	ext_s_args;

#endif
