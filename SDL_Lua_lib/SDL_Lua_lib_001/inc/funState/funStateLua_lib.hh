#pragma	once

#ifndef		__FUNSTATELUA_HH__
# define	__FUNSTATELUA_HH__

#include	<lua/lua.hpp>

int					(SDL_GetError_lua)			( lua_State * L );
int					(SDL_SetVideoMode_lua)		( lua_State * L );
int					(SDL_FillRect_lua)			( lua_State * L );
int					(SDL_CreateRGBSurface_lua)	( lua_State * L );
int					(SDL_LoadBMP_lua)			( lua_State * L );
int					(IMG_Load_lua)				( lua_State * L );
int					(IMG_GetError_lua)			( lua_State * L );
int					(SDL_FreeSurface_lua)		( lua_State * L );
int					(SDL_BlitSurface_lua)		( lua_State * L );
int					(SDL_MapRGB_lua)			( lua_State * L );
int					(SDL_SetColorKey_lua)		( lua_State * L );
int					(SDL_SetAlpha_lua)			( lua_State * L );
int					(SDL_Flip_lua)				( lua_State * L );
int					(SDL_WM_SetCaption_lua)		( lua_State * L );
int					(SDL_WM_SetIcon_lua)		( lua_State * L );
int					(SDL_WaitEvent_lua)			( lua_State * L );
int					(SDL_PollEvent_lua)			( lua_State * L );
int					(SDL_Delay_lua)				( lua_State * L );

#endif
