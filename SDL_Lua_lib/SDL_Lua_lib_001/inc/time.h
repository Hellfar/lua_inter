#pragma	once

#ifndef		__TIME_H__
# define	__TIME_H__

# if		defined(_WIN32) || defined(_WIN64)
//#  define	asctime			asctime_s
# elif		defined(unix) || defined(__unix) || defined(__unix__) || (defined(__gnuc__) && defined(_UNIX))
// #  include	<ctime>
# else
#  error	System not suported.
# endif

#endif
