#!/bin/sh

export repository="build_UNIX"

mkdir "$repository"
cd "$repository"
cmake ..
make
cd ..
# PAUSE
