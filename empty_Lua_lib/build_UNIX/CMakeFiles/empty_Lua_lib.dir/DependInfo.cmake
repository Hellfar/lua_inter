# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/hellfar/Projects/C++/lua_inter/cross/empty_Lua_lib/src/funState/funStateLua_lib.cpp" "/home/hellfar/Projects/C++/lua_inter/cross/empty_Lua_lib/build_UNIX/CMakeFiles/empty_Lua_lib.dir/src/funState/funStateLua_lib.cpp.o"
  "/home/hellfar/Projects/C++/lua_inter/cross/empty_Lua_lib/src/funState/register.cpp" "/home/hellfar/Projects/C++/lua_inter/cross/empty_Lua_lib/build_UNIX/CMakeFiles/empty_Lua_lib.dir/src/funState/register.cpp.o"
  "/home/hellfar/Projects/C++/lua_inter/cross/empty_Lua_lib/src/main.cpp" "/home/hellfar/Projects/C++/lua_inter/cross/empty_Lua_lib/build_UNIX/CMakeFiles/empty_Lua_lib.dir/src/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BUILD_DLL"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../inc"
  "../lib/inc"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
