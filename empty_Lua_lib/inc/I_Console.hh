#pragma	once

#ifndef		__I_CONSOLE_HH__
# define	__I_CONSOLE_HH__

# include	<string>
# include	"conf.hh"

class					I_Console
{	
public:
	virtual (~I_Console)								(){};
	
public:
	virtual std::string const &	(getTitle)				( void ) const = 0;
	virtual void				(setTitle)				( std::string const & title ) = 0;
	virtual void				(setDefaultTextColor)	( int color = DEFAULT_COLOR_CONSOLE ) = 0;
	
public:
	virtual void				(outflow)				( std::string const & str, int color = -1 ) const = 0;
	virtual std::string			(incflow)				( int max = -1 ) const = 0;
};

#endif