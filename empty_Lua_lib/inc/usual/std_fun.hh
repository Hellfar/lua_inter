#pragma once

#ifndef		__FUN_H__
# define	__FUN_H__

void					(clean_buffer)	( char *buf );
std::string &			(str_replace)	( const std::string &search, const std::string &replace, std::string &subject );
bool					(is_readable)	( const std::string & file );
std::string				(solve_path)	( std::string const & l, std::string const & r );
std::string				(path_parse)	( std::string const & path, std::string const & search );

#endif