#pragma	once

#ifndef		__PRINTLOG_H__
# define	__PRINTLOG_H__

# define	DEFAULT_LOGNAME			"report.log"

namespace	std
{
	void				(init_printLog)	( char const * fileName = DEFAULT_LOGNAME );

	ostream &			(log)			( ostream & os );
	ostream &			(warning)		( ostream & os );
	ostream &			(error)			( ostream & os );
	ostream &			(raise)			( ostream & os );
	ostream &			(ctime)			( ostream & os );
};

#endif