#pragma	once

#ifndef		__UNISTD_CROSS_H__
# define	__UNISTD_CROSS_H__

# ifdef	_WIN32
#  include	<direct.h>
#  define	chdir		_chdir
#  define	getcwd		_getcwd
# elif		defined(__GNUC__) && defined(__unix__) && defined(__linux__)
#  include	<unistd.h>
# endif

#endif
