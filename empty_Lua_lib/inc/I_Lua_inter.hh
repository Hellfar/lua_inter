#pragma	once

#ifndef		__I_LUA_INTER_HH__
# define	__I_LUA_INTER_HH__

// # include	<vector>
# include	<map>
# include	<string>
# include	<lua/lua.hpp>
# include	"conf.hh"
# include	"I_Console.hh"

# ifdef	_WIN32

#  include	<windows.h>

#  define	TYPELIB		HMODULE

# else

#  define	TYPELIB		void *

# endif

class					I_Lua_inter
{
public:
	virtual (~I_Lua_inter)								(){};
	
public:
	virtual bool					(run)				( std::string const & file = DEFAULT_FILENAME ) = 0;
	virtual void					(load_stdLibs)		( void ) = 0;
	
public:
	virtual lua_State *				(getState)			( void ) const = 0;
	virtual char const * const *	(getArgv)			( int * argc ) const = 0;
	virtual int						(getReturn)			( void ) const = 0;
	virtual std::string const &		(getCurrentDir)		( void ) const = 0;
	virtual std::string const &		(getCurrentFile)	( void ) const = 0;
	//virtual std::vector<TYPELIB> &	(getALibRef)		( void ) = 0;
	virtual int						(addLibRef)			( std::string const & nameLib, std::string const & sparams = "" ) = 0;
	virtual void					(eraseLibRef)		( unsigned int pos ) = 0;
	virtual void					(clearALibRef)		( void ) = 0;
# ifdef  _WIN32
	virtual I_Console *				(getConsole)		( void ) const = 0;
	virtual void					(setConsole)		( bool state ) = 0;
# endif
};

extern std::map< lua_State *, I_Lua_inter * >	convLink;

#endif