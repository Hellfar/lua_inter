#pragma	once

#ifndef		__UNDER_FUNSTATELUA_HPP__
# define	__UNDER_FUNSTATELUA_HPP__

# include	<string>
# include	<lua/lua.hpp>

void					(ur_listDir)	( lua_State * L, std::string const & path, bool recurs );

#endif