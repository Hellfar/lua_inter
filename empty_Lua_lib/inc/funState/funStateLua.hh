#pragma once

#ifndef		__FUNSTATELUA_HPP__
# define	__FUNSTATELUA_HPP__

# include	<lua/lua.hpp>

int						(include_luaFile)	( lua_State * L );
int						(start_lua)			( lua_State * L );
int						(stop_lua)			( lua_State * L );
int						(eval_lua)			( lua_State * L );
int						(load_fun_lua)		( lua_State * L );
int						(report_lua)		( lua_State * L );
int						(throw_lua)			( lua_State * L );

int						(listDir_lua)		( lua_State * L );

# ifdef	_WIN32
int						(load_libFile)		( lua_State * L );
int						(unload_libFile)	( lua_State * L );
int						(allowConsole)		( lua_State * L );
int						(getTitleConsole)	( lua_State * L );
int						(setTitleConsole)	( lua_State * L );
int						(setSectColCsl)		( lua_State * L );
int						(outflow)			( lua_State * L );
int						(incflow)			( lua_State * L );
int						(pause)				( lua_State * L );
# endif

#endif
