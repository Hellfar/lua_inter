#include	<string>
#include	<lua/lua.hpp>
//include library here.
#include	"usual/printLog.hh"
#include	"funState/funStateLua_lib.hh"
#include	"funState/register.hh"
#include	"I_Lua_inter.hh"

#include	"main.hh"

bool							empty_Lua_lib = false;

bool DLL_EXPORT					processAtLoad( I_Lua_inter * oprgm, std::string const & paramForLoad )
{
	(void)oprgm;
    (void)paramForLoad;
	
	// do something at load.
	
	return (empty_Lua_lib);
}

bool DLL_EXPORT					processAtUnload( I_Lua_inter * oprgm, std::string const & paramForLoad )
{
	(void)oprgm;
    (void)paramForLoad;
	
	// do something at unload.

	return (true);
}
