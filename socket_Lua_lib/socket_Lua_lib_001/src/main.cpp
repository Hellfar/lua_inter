#ifdef		_WIN32
# pragma	comment("_CRT_SECURE_NO_WARNINGS")
#endif

#ifdef		_WIN32
# include	<winsock2.h>
#endif
#include	<string>
#include	<lua/lua.hpp>
#include	"usual/printLog.hh"
#include	"funState/funStateLua_lib.hh"
#include	"funState/register.hh"
#include	"I_Lua_inter.hh"

#include	"main.hh"

bool							socket_Lua_lib = false;

bool DLL_EXPORT					processAtLoad( I_Lua_inter * oprgm, std::string const & paramForLoad )
{
	if (!socket_Lua_lib)
	{
#if			defined(_WIN32)
		WSADATA					wsa;
		
		if(::WSAStartup(MAKEWORD(2, 2), &wsa) >= 0)
		{
#endif
			socket_Lua_lib = true;
			::registerToLua_lib(oprgm->getState());
#if			defined(_WIN32)
		}
#endif
	}
	
	return (socket_Lua_lib);
}

bool DLL_EXPORT					processAtUnload( I_Lua_inter * oprgm, std::string const & paramForLoad )
{
	(void)paramForLoad;
	if (socket_Lua_lib)
	{
#if			defined(_WIN32)
		::WSACleanup();
#endif
		::unregisterToLua_lib(oprgm->getState());
	}
	
	return (true);
}
