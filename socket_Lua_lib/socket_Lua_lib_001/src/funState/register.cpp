#include	<lua/lua.hpp>
#include	"main.hh"
#include	"funState/funStateLua_lib.hh"

#include	"funState/register.hh"

void				registerToLua_lib( lua_State * L )
{
	// func ----------------------------------------------------------------
	
	// register your function like this.
	//~ lua_register(L, "client", &client_test);
	
	
	// const ----------------------------------------------------------------
	
	// define your constant like this.
	//lua_setConst(L, SDL_SWSURFACE);
}

void				unregisterToLua_lib( lua_State * L )
{
	// func ----------------------------------------------------------------
	
	// Unload your function lib like this.
	//lua_unregister(L, "SDL_GetError");
	
	// const ----------------------------------------------------------------
	
	// Unload your constant like this.
	//lua_unregister(L, "SDL_SWSURFACE");
}
