#include <cstring>
#include <string>

#include "container/TCP.hh"

TCP::TCP()
{
    if((this->sock = ::socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
    {
        this->statusSocket = Socket::ERRORSOCKET;
    }
}

bool                            TCP::connect( std::string const & hostname, unsigned int port )
{
    struct hostent *            hostinfo = ::gethostbyname(hostname.c_str());
    SOCKADDR_IN                 sin;

    ::memset(&sin, 0, sizeof (SOCKADDR_IN));

    if (hostinfo == NULL)
    {
        this->statusSocket = Socket::UNCONNECTEDSOCKET;

        return (false);
    }

    sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
    sin.sin_port = htons(port);
    sin.sin_family = AF_INET;

    if (::connect(sock, (SOCKADDR *)&sin, sizeof (SOCKADDR)) == SOCKET_ERROR)
    {
        this->statusSocket = Socket::UNCONNECTEDSOCKET;

        return (false);
    }

    return (true);
}

bool                            TCP::recv( void * data, size_t noctets )
{
    int n;

    if((n = ::recv(this->sock, static_cast< char * > (data), noctets, 0)) < 0)
    {
        this->statusSocket = Socket::UNRECEIVEDATA;

        return (false);
    }

    return (true);
}

bool                            TCP::recv( std::string & str, size_t len )
{
    char *                      buffer = new (std::nothrow) char [len + 1];
    int                         n;

    if((n = ::recv(sock, buffer, len, 0)) < 0)
    {
        this->statusSocket = Socket::UNRECEIVEDATA;

        return (false);
    }
    buffer[n] = '\0';
    str = buffer;
    delete [] buffer;

    return (true);
}

bool                            TCP::send( void const * data, size_t noctets )
{
    if(::send(this->sock, static_cast< char const * > (data), noctets, 0) < 0)
    {
        this->statusSocket = Socket::UNSENDDATA;

        return (false);
    }

    return (true);
}

bool                            TCP::send( std::string const & str )
{
    if(::send(this->sock, str.c_str(), str.length(), 0) < 0)
    {
        this->statusSocket = Socket::UNSENDDATA;

        return (false);
    }

    return (true);
}
