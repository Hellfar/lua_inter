#pragma	once

#ifndef		__FUNSTATELUA_HH__
# define	__FUNSTATELUA_HH__

#include	<lua/lua.hpp>

int					(connectSock_lua)	( lua_State * L );
int					(closeSock_lua)		( lua_State * L );
int					(send_lua)			( lua_State * L );
int					(recv_lua)			( lua_State * L );

#endif
