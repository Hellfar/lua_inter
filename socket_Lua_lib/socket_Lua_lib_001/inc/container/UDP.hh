#pragma once

#ifndef     __UDP_HH__
# define    __UDP_HH__

#include "Socket.hh"

class                       UDP:    public  Socket
{
    public:
    UDP();

    public:
    virtual bool            connect( std::string const & hostname );
};

#endif

