#pragma once

#ifndef     __SOCKET_HH__
# define    __SOCKET_HH__

# include	<string>

# if		defined(_WIN32)
#  include	<winsock2.h>
# elif		defined(__GNUC__) && defined(__unix__) && defined(__linux__)
#  include	<sys/types.h>
#  include	<sys/socket.h>
#  include	<netinet/in.h>
#  include	<arpa/inet.h>
#  include	<unistd.h> /* close */
#  include	<netdb.h> /* gethostbyname */
#  define	INVALID_SOCKET -1
#  define	SOCKET_ERROR -1
#  define	closesocket(s) close(s)
typedef	int					SOCKET;
typedef	struct sockaddr_in	SOCKADDR_IN;
typedef	struct sockaddr		SOCKADDR;
typedef	struct in_addr		IN_ADDR;
# else
#  error not defined for this platform
# endif

class                       Socket
{
    public:
    enum    e_statusSocket
    {
        GOOD,
        ERRORSOCKET,
        UNCONNECTEDSOCKET,
        UNSENDDATA,
        UNRECEIVEDATA,
    };

    static unsigned int const   defaultPort = 80;
    static unsigned int const   maxReceiveData = 80;

    protected:
    enum e_statusSocket     statusSocket;

    protected:
    SOCKET                  sock;

    public:
    Socket();
    virtual ~Socket();

    public:
    //virtual bool            connect( unsigned int IPaddr, unsigned int port = Socket::defaultPort ) = 0;
    virtual bool            connect( std::string const & hostname, unsigned int port = Socket::defaultPort ) = 0;
    virtual bool            recv( void * data, size_t noctets = Socket::maxReceiveData ) = 0;
    virtual bool            recv( std::string & str, size_t noctets = Socket::maxReceiveData ) = 0;
    virtual bool            send( void const * data, size_t noctets ) = 0;
    virtual bool            send( std::string const & str ) = 0;
};

#endif
