#pragma once

#ifndef     __TCP_HH__
# define    __TCP_HH__

# include	<string>

# include	"container/Socket.hh"

class                       TCP:    public  Socket
{
    public:
    TCP();

    public:
    virtual bool            connect( std::string const & hostname, unsigned int port );
    virtual bool            recv( void * data, size_t noctets );
    virtual bool            recv( std::string & str, size_t noctets );
    virtual bool            send( void const * data, size_t noctets );
    virtual bool            send( std::string const & str );
};

#endif

